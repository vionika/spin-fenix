function parseQuery(queryString) {
   if (queryString[0] === '?') {
       queryString = queryString.substr(1);
   }
   const query = Object.fromEntries(new URLSearchParams(queryString).entries());
   injectValues(query)
};

function injectValues(queryMap) {
    document.getElementById('title').innerHTML = queryMap.title
    document.getElementById('subtitle').innerHTML = queryMap.subtitle
    document.getElementById('description').innerHTML = queryMap.description
    document.getElementById('smallDescription').innerHTML = queryMap.smallDescription
    document.getElementById('upgradeInfo').innerHTML = queryMap.upgradeInfo
    document.getElementById('upgradeButton').innerHTML = queryMap.upgradeButton
    document.getElementById('urlValue').value = queryMap.urlValue

    if (queryMap.hasSubscription == 1) {
        document.getElementById('upgradeInfo').style.display = 'none';
        document.getElementById('separatorHorizontal').style.display = 'none';
        document.getElementById('upgradeButton').style.display = 'none';
    }
}

function openUrlAction(action) {
    const urlValue = document.getElementById('urlValue').value;
    window.location.href = 'spin-action://' + action + '?url=' + encodeURIComponent(urlValue);
}

function upgrade() {
    window.location.href = 'spin-action://upgrade'
}

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('allowButton').addEventListener('click', () => openUrlAction('allow'));
    document.getElementById('blockButton').addEventListener('click', () => openUrlAction('block'));
    document.getElementById('upgradeButton').addEventListener('click', () => upgrade());
});

parseQuery(document.documentURI);
