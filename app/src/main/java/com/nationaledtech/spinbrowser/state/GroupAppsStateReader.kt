package com.nationaledtech.spinbrowser.state

import android.content.Context
import android.net.Uri

class GroupAppsStateReader {
    private val GROUP_APPS_STATE_CONTENT_URL = "content://%s.db.browser/group_apps_state"

    fun hasActiveApp(context: Context): Boolean {
        val policyContentUrl = GROUP_APPS_STATE_CONTENT_URL.format(context.packageName)
        val cursor = context.contentResolver.query(
            Uri.parse(policyContentUrl),
            arrayOf("state"),
            "state = 1",
            null,
            null
        )

        cursor.use {
            if (it != null && it.moveToFirst()) {
                return true;
            }
        }

        return false
    }
}
