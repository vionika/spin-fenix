package com.nationaledtech.spinbrowser.pages

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import mozilla.components.support.ktx.kotlin.urlEncode
import org.mozilla.fenix.R

object SpinErrorPages {
    private const val UNKNOWN_WEBSITE_RESOURCE_FILE = "spin_unknown_website.html"

    @SuppressLint("StringFormatInvalid")
    fun createUrlEncodedUnknownWebsitePage(
        context: Context,
        url: String,
        hasSubscription: Boolean
    ): String {
        val title = context.getString(R.string.spin_errors_unknown_website_title)

        val cleanUrl = Uri.parse(url).host
        val subtitle = context.getString(R.string.spin_errors_unknown_website_subtitle, cleanUrl)
        val description = context.getString(R.string.spin_errors_unknown_website_description)
        val smallDescription =
            context.getString(R.string.spin_errors_unknown_website_description_small)
        val upgradeInfo = context.getString(R.string.spin_errors_unknown_website_upgrade_info)
        val upgradeButton =
            context.getString(R.string.spin_errors_unknown_website_upgrade_button_title)

        return "resource://android/assets/spin/errors/unknownwebsite/$UNKNOWN_WEBSITE_RESOURCE_FILE?" +
                "&title=${title.urlEncode()}" +
                "&subtitle=${subtitle.urlEncode()}" +
                "&description=${description.urlEncode()}" +
                "&smallDescription=${smallDescription.urlEncode()}" +
                "&upgradeInfo=${upgradeInfo.urlEncode()}" +
                "&upgradeButton=${upgradeButton.urlEncode()}" +
                "&urlValue=${url.urlEncode()}" +
                "&hasSubscription=${if (hasSubscription) 1 else 0}"
    }
}
