package com.nationaledtech.spinbrowser.onboarding

import android.view.View
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import org.mozilla.fenix.R
import org.mozilla.fenix.databinding.OnboardingFilterChromeAdvertBinding
import android.content.Intent
import android.net.Uri
import com.nationaledtech.spinbrowser.SpinSupportUtils

class OnboardingFilterChromeViewHolder (
    view: View
) : RecyclerView.ViewHolder(view) {

    init {
        val binding = OnboardingFilterChromeAdvertBinding.bind(view)

        binding.descriptionText.text = HtmlCompat.fromHtml(
            view.context.getString(
                R.string.onboarding_filter_chrome_description
            ),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )

        binding.installButton.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(SpinSupportUtils.getFilterChromeGoolePlayUrl()))
            view.context.startActivity(browserIntent)
        }
    }

    companion object {
        const val LAYOUT_ID = R.layout.onboarding_filter_chrome_advert
    }
}
