package com.nationaledtech.spinbrowser.onboarding

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.mozilla.fenix.R
import androidx.navigation.Navigation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.mozilla.fenix.FenixApplication
import org.mozilla.fenix.databinding.OnboardingSpinPlusAdvertBinding
import org.mozilla.fenix.ext.application
import org.mozilla.fenix.home.HomeFragmentDirections
import org.mozilla.fenix.onboarding.OnboardingFragmentDirections
import org.mozilla.fenix.settings.SettingsFragmentDirections

class OnboardingSpinPlusViewHolder (
    view: View
) : RecyclerView.ViewHolder(view) {

    init {
        val binding = OnboardingSpinPlusAdvertBinding.bind(view)

        binding.installButton.setOnClickListener {
            val directions = OnboardingFragmentDirections.actionSettings()
            Navigation.findNavController(view).navigate(directions)

            var hasSubscription = false
            val manager = (view.context?.application)?.subscriptionManager
            if (manager != null) {
                runBlocking(Dispatchers.IO) {
                    hasSubscription = manager.userCurrentSubscriptionFlow.first().hasSubscription
                }
            }
            val subscriptionDirections = if (hasSubscription) SettingsFragmentDirections.actionSettingsFragmentToConfigureSpinFragment()
            else SettingsFragmentDirections.actionSettingsFragmentToSubscribeSpinPLus()
            Navigation.findNavController(view).navigate(subscriptionDirections)
        }
    }

    companion object {
        const val LAYOUT_ID = R.layout.onboarding_spin_plus_advert
    }
}
