package com.nationaledtech.spinbrowser.onboarding

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.nationaledtech.spinbrowser.SpinSupportUtils
import org.mozilla.fenix.R
import org.mozilla.fenix.databinding.OnboardingBoomerangAdvertBinding

class OnboardingBoomerangViewHolder (
    view: View
) : RecyclerView.ViewHolder(view) {

    init {
        val binding = OnboardingBoomerangAdvertBinding.bind(view)

        binding.descriptionText.text = HtmlCompat.fromHtml(
            view.context.getString(
                R.string.onboarding_boomerang_description
            ),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )

        binding.installButton.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(SpinSupportUtils.getBoomerangGoolePlayUrl()))
            view.context.startActivity(browserIntent)
        }
    }

    companion object {
        const val LAYOUT_ID = R.layout.onboarding_boomerang_advert
    }
}
