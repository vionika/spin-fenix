package com.nationaledtech.spinbrowser.services

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.json.responseJson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class ExamineDomainService {
    private val serviceUrl = "https://www.vionika.com/services/examine/domain"
    private val apiKey = "oz2erssx768cHfzDMOO1PsyIz2EaJsyDppqrwmHckoHsrGBOJ2tPkA=="
    private val job = Job()
    private val ioScope = CoroutineScope(Dispatchers.IO + job)

    fun examineDomain(domain: String, completion: (Array<Int>) -> Unit) {
        ioScope.launch {
            val jsonBody = String.format(
                "{\"domainName\":\"%s\", \"key\":\"%s\", \"v\":\"1\"}",
                domain,
                apiKey
            )

            val (_, response, result) = Fuel.post(serviceUrl)
                .header("Content-Type" to "application/json", "Accept" to "application/json")
                .body(jsonBody)
                .responseJson()

            if (response.responseMessage == "OK" && response.statusCode == 200) {
                val categories = result.get().obj().getJSONArray("categories")
                val result = Array(categories.length()) {
                    categories.getInt(it)
                }
                ioScope.launch(Dispatchers.Main) {
                    completion(result)
                }
            } else {
                ioScope.launch(Dispatchers.Main) {
                    completion(arrayOf())
                }
            }
        }
    }
}