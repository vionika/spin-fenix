package com.nationaledtech.spinbrowser

import android.content.ContentValues
import android.content.Context
import com.nationaledtech.spinbrowser.db.SpinDatabaseContract
import mozilla.components.concept.engine.history.HistoryTrackingDelegate
import mozilla.components.concept.storage.HistoryStorage
import mozilla.components.concept.storage.PageVisit
import mozilla.components.feature.session.HistoryDelegate
import java.net.URI
import java.util.*

class SpinHistoryDelegate(
        private val context: Context,
        private val historyStorage: Lazy<HistoryStorage>
) : HistoryTrackingDelegate {
    private val historyTrackingDelegate = HistoryDelegate(historyStorage)

    override suspend fun getVisited(): List<String> {
        return historyTrackingDelegate.getVisited()
    }

    override suspend fun getVisited(uris: List<String>): List<Boolean> {
        return historyTrackingDelegate.getVisited(uris)
    }

    override suspend fun onPreviewImageChange(uri: String, previewImageUrl: String) {
        historyTrackingDelegate.onPreviewImageChange(uri, previewImageUrl)
    }

    override suspend fun onTitleChanged(uri: String, title: String) {
        if (!shouldStoreUri(uri)) return

        val date = Date().time
        val contentValues = ContentValues(4)
        contentValues.put(SpinDatabaseContract.HistoryEntry.COLUMN_TITLE, title)
        contentValues.put(SpinDatabaseContract.HistoryEntry.COLUMN_URL, uri)
        contentValues.put(SpinDatabaseContract.HistoryEntry.COLUMN_DATE, date)
        contentValues.put(SpinDatabaseContract.HistoryEntry.COLUMN_MODIFIED, date)
        context.contentResolver.insert(SpinDatabaseContract.HistoryEntry.CONTENT_URI, contentValues)

        historyTrackingDelegate.onTitleChanged(uri, title)
    }

    override suspend fun onVisited(uri: String, visit: PageVisit) {
        if (!shouldStoreUri(uri)) return

        historyTrackingDelegate.onVisited(uri, visit)
    }

    override fun shouldStoreUri(uri: String): Boolean {
        return historyTrackingDelegate.shouldStoreUri(uri)
    }
}