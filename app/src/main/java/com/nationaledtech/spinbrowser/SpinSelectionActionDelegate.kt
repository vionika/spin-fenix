package com.nationaledtech.spinbrowser

import android.content.res.Resources
import android.util.Patterns
import androidx.annotation.VisibleForTesting
import mozilla.components.concept.engine.selection.SelectionActionDelegate
import mozilla.components.feature.contextmenu.facts.ContextMenuFacts
import mozilla.components.feature.search.SearchAdapter
import mozilla.components.support.base.Component
import mozilla.components.support.base.facts.Action
import mozilla.components.support.base.facts.Fact
import mozilla.components.support.base.facts.collect
import org.mozilla.fenix.R

@VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
internal const val SEARCH = "CUSTOM_CONTEXT_MENU_SEARCH"
@VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
internal const val SHARE = "CUSTOM_CONTEXT_MENU_SHARE"
@VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
internal const val EMAIL = "CUSTOM_CONTEXT_MENU_EMAIL"
@VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
internal const val CALL = "CUSTOM_CONTEXT_MENU_CALL"

private val customActions = arrayOf(CALL, EMAIL, SEARCH, SHARE)

/**
 * Adds normal and private search buttons to text selection context menus.
 * Also adds share, email, and call actions which are optionally displayed.
 */
@Suppress("LongParameterList")
class SpinSelectionActionDelegate(
    private val searchAdapter: SearchAdapter,
    resources: Resources,
    private val shareTextClicked: ((String) -> Unit)? = null,
    private val emailTextClicked: ((String) -> Unit)? = null,
    private val callTextClicked: ((String) -> Unit)? = null,
    private val actionSorter: ((Array<String>) -> Array<String>)? = null
) : SelectionActionDelegate {

    private val normalSearchText =
        resources.getString(R.string.mozac_selection_context_menu_search_2)
    private val privateSearchText =
        resources.getString(R.string.mozac_selection_context_menu_search_privately_2)
    private val shareText = resources.getString(R.string.mozac_selection_context_menu_share)
    private val emailText = resources.getString(R.string.mozac_selection_context_menu_email)
    private val callText = resources.getString(R.string.mozac_selection_context_menu_call)

    override fun getAllActions(): Array<String> = customActions

    @SuppressWarnings("ComplexMethod")
    override fun isActionAvailable(id: String, selectedText: String): Boolean {
        return (id == SHARE && shareTextClicked != null) ||
                (
                        id == EMAIL && emailTextClicked != null &&
                                Patterns.EMAIL_ADDRESS.matcher(selectedText.trim()).matches()
                        ) ||
                (
                        id == CALL &&
                                callTextClicked != null && Patterns.PHONE.matcher(selectedText.trim()).matches()
                        ) ||
                (id == SEARCH)
    }

    override fun getActionTitle(id: String): CharSequence? = when (id) {
        SEARCH -> normalSearchText
        SHARE -> shareText
        EMAIL -> emailText
        CALL -> callText
        else -> null
    }

    override fun performAction(id: String, selectedText: String): Boolean {
        emitTextSelectionClickFact(id)
        return when (id) {
            SEARCH -> {
                searchAdapter.sendSearch(false, selectedText)
                true
            }
            SHARE -> {
                shareTextClicked?.invoke(selectedText)
                true
            }
            EMAIL -> {
                emailTextClicked?.invoke(selectedText.trim())
                true
            }
            CALL -> {
                callTextClicked?.invoke(selectedText.trim())
                true
            }
            else -> {
                false
            }
        }
    }

    /**
     * Takes in a list of actions and sorts them.
     * @returns the sorted list.
     */
    override fun sortedActions(actions: Array<String>): Array<String> {
        val safeActions = actions.filter {
                   it.startsWith("org.mozilla.geckoview")
                || it.endsWith("MENU_CALL")
                || it.endsWith("MENU_EMAIL")
                || it.endsWith("MENU_SEARCH")
                || it.endsWith("MENU_SHARE")}.toTypedArray()
        return if (actionSorter != null) {
            actionSorter.invoke(safeActions)
        } else {
            safeActions
        }
    }
}

internal fun emitTextSelectionClickFact(optionId: String) {
    val metadata = mapOf("textSelectionOption" to optionId)
    emitContextMenuFact(Action.CLICK, ContextMenuFacts.Items.TEXT_SELECTION_OPTION, metadata = metadata)
}

private fun emitContextMenuFact(
    action: Action,
    item: String,
    value: String? = null,
    metadata: Map<String, Any>? = null
) {
    Fact(
        Component.FEATURE_CONTEXTMENU,
        action,
        item,
        value,
        metadata
    ).collect()
}