package com.nationaledtech.spinbrowser.plus.domains.view

import androidx.recyclerview.widget.LinearLayoutManager
import com.nationaledtech.spinbrowser.plus.domains.ManagedDomainsState
import com.nationaledtech.spinbrowser.plus.domains.interactor.ManagedDomainsInteractor
import org.mozilla.fenix.R
import org.mozilla.fenix.databinding.ComponentDomainsBinding

class ManagedDomainsView(
    val binding: ComponentDomainsBinding,
    val interactor: ManagedDomainsInteractor
) {

    private val domainsAdapter = ManagedDomainsAdapter(interactor)

    init {
        binding.domainsList.apply {
            adapter = domainsAdapter
            layoutManager = LinearLayoutManager(binding.root.context)
        }

        binding.addDomainButton.addDomainLayout.setOnClickListener { interactor.onAddDomainClick() }
    }

    fun update(state: ManagedDomainsState) {
        domainsAdapter.submitList(state.domainsList)
    }

    companion object {
        const val LAYOUT_ID = R.layout.component_domains
    }
}
