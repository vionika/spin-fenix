package com.nationaledtech.spinbrowser.plus.domains

data class ManagedDomainEditorState(
    val isAllowed: Boolean = false
)