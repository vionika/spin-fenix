package com.nationaledtech.spinbrowser.plus.domains

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.nationaledtech.spinbrowser.plus.domains.controller.DefaultManagedDomainsController
import com.nationaledtech.spinbrowser.plus.domains.interactor.DefaultManagedDomainsInteractor
import com.nationaledtech.spinbrowser.plus.domains.interactor.ManagedDomainsInteractor
import com.nationaledtech.spinbrowser.plus.domains.view.ManagedDomainsView
import com.nationaledtech.spinbrowser.plus.passcode.PasscodeLockedFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mozilla.components.lib.state.ext.consumeFrom
import org.mozilla.fenix.NavHostActivity
import org.mozilla.fenix.R
import org.mozilla.fenix.components.StoreProvider
import org.mozilla.fenix.databinding.ComponentDomainsBinding
import org.mozilla.fenix.ext.showToolbar

class ManagedDomainsFragment : Fragment() {
    private lateinit var storage: ManagedDomainsStorage
    private lateinit var store: ManagedDomainsStore
    private lateinit var interactor: ManagedDomainsInteractor
    private lateinit var domainsView: ManagedDomainsView

    private val args by navArgs<ManagedDomainsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(ManagedDomainsView.LAYOUT_ID, container, false)

        storage = DefaultManagedDomainsStorage(requireContext())

        store = StoreProvider.get(this) {
            ManagedDomainsStore(ManagedDomainsState(listOf()))
        }

        interactor = DefaultManagedDomainsInteractor(
            controller = DefaultManagedDomainsController(
                isAllowed = args.isAllowed,
                store = store,
                storage = storage,
                lifecycleScope = lifecycleScope,
                navController = findNavController()
            ),
        )
        val binding = ComponentDomainsBinding.bind(view)

        domainsView = ManagedDomainsView(binding, interactor)

        loadDomains()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        findNavController().previousBackStackEntry?.savedStateHandle?.set("isPopBackStack", true)

        consumeFrom(store) { state ->
            domainsView.update(state)
        }
    }

    override fun onResume() {
        super.onResume()

        (activity as NavHostActivity).getSupportActionBarAndInflateIfNecessary().show()

        if (args.isAllowed) {
            showToolbar(getString(R.string.preferences_spin_plus_domains_allowed_list))
        } else {
            showToolbar(getString(R.string.preferences_spin_plus_domains_blocked_list))
        }
    }

    private fun loadDomains() {
        lifecycleScope.launch(Dispatchers.IO) {
            val domains = storage.getAllDomains(args.isAllowed)
            lifecycleScope.launch(Dispatchers.Main) {
                store.dispatch(ManagedDomainsAction.UpdateDomains(domains))
            }
        }
    }
}
