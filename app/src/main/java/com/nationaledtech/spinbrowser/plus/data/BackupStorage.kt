package com.nationaledtech.spinbrowser.plus.data

import android.content.Context
import android.net.Uri
import android.util.Log
import com.google.gson.GsonBuilder
import com.nationaledtech.spinbrowser.plus.data.model.BackupData
import org.bson.BsonBinaryReader
import org.bson.BsonBinaryWriter
import org.bson.Document
import org.bson.codecs.DecoderContext
import org.bson.codecs.DocumentCodec
import org.bson.codecs.EncoderContext
import org.bson.io.BasicOutputBuffer
import java.io.*
import java.nio.ByteBuffer
import java.text.SimpleDateFormat
import java.util.*

class BackupStorage(
    val context: Context,
) {
    companion object {
        const val BACKUP_VERSION = 1.0
        const val BACKUPS_DIRECTORY_NAME = "data_backups"
        const val BACKUP_FILE_EXTENSION = "spnbkp"

        const val BUFFER_SIZE = 1024 * 4;
    }

    private val gson = GsonBuilder().setVersion(BACKUP_VERSION).create()
    private var codec = DocumentCodec()
    private val fileProviderAuthority = context.packageName

    fun storeBackupToFile(context: Context, data: BackupData, uri: Uri): Boolean {
        try {
            val fileOutupStream =
                context.contentResolver.openOutputStream(uri)
            BufferedOutputStream(fileOutupStream).use { output ->
                val bson = Document.parse(gson.toJson(data))
                bsonToInputStream(bson)?.let { copyData(it, output) }
                return true
            }
        } catch (e: Exception) {
            Log.e("BackupStorage", "Error storing backup: %s", e)
            return false
        }
    }

    fun retrieveBackupFromUri(uri: Uri): BackupData? {
        try {
            context.contentResolver.openFileDescriptor(uri, "r").use { parcelFileDescriptor ->
                if (parcelFileDescriptor == null) {
                    return null
                }
                val backupFileDescriptor = parcelFileDescriptor.fileDescriptor
                FileInputStream(backupFileDescriptor).use { backupStream ->
                     bsonFromInputStream(backupStream)?.let { personObj ->
                         return gson.fromJson(personObj.toJson(), BackupData::class.java)
                     }
                }
            }
        } catch (e: java.lang.Exception) {
            Log.e("BackupStorage", "Cannot open selected backup file: %s", e)
        } catch (e: IOException) {
            Log.e("BackupStorage", "Cannot read backup file as BSON: %s", e)
        }

        return null
    }

    private fun bsonToInputStream(document: Document): InputStream? {
        val outputBuffer = BasicOutputBuffer()
        val writer = BsonBinaryWriter(outputBuffer)
        codec.encode(
            writer,
            document,
            EncoderContext.builder().isEncodingCollectibleDocument(true).build(),
        )
        return ByteArrayInputStream(outputBuffer.toByteArray())
    }

    private fun bsonFromInputStream(input: InputStream): Document? {
        val outputStream = ByteArrayOutputStream()
        val buffer = ByteArray(1024)
        var len: Int
        while (input.read(buffer).also { len = it } != -1) {
            outputStream.write(buffer, 0, len)
        }
        outputStream.close()
        val bsonReader = BsonBinaryReader(ByteBuffer.wrap(outputStream.toByteArray()))
        return codec.decode(bsonReader, DecoderContext.builder().build())
    }

    private fun copyData(input: InputStream, output: OutputStream): Long {
        val buf = ByteArray(BUFFER_SIZE)
        var count: Long = 0
        var n = 0
        while (input.read(buf).also { n = it } > -1) {
            output.write(buf, 0, n)
            count += n.toLong()
        }
        return count
    }

    private fun generateBackupFileName(): String? {
        return SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
            .format(Date()) + "." + BACKUP_FILE_EXTENSION
    }
}
