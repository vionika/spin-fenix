package com.nationaledtech.spinbrowser.plus.passcode

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.beautycoder.pflockscreen.PFFLockScreenConfiguration
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment
import com.beautycoder.pflockscreen.security.PFSecurityManager
import com.nationaledtech.spinbrowser.SpinSupportUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.mozilla.fenix.NavHostActivity
import org.mozilla.fenix.R

class PasscodeFragment : Fragment(R.layout.fragment_passcode) {
    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }
    override fun onDestroy() {
        super.onDestroy()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
    }
    override fun onResume() {
        super.onResume()

        (activity as NavHostActivity).getSupportActionBarAndInflateIfNecessary().show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findNavController().previousBackStackEntry?.savedStateHandle?.set("isPopBackStack", true)

        val isPasscodeSet = SpinSupportUtils.isPasscodeSet(view.context)
        val fragment =
            if (isPasscodeSet) buildDisablePasscodeFragment() else buildEnablePasscodeFragment()

        activity?.supportFragmentManager
            ?.beginTransaction()
            ?.add(R.id.passcode_container, fragment, "fragmentId")
            ?.commit()
    }

    private fun buildEnablePasscodeFragment(): Fragment {
        val context = requireContext()
        val fragment = PFLockScreenFragment()
        val builder: PFFLockScreenConfiguration.Builder =
            PFFLockScreenConfiguration.Builder(requireContext())
                .setMode(PFFLockScreenConfiguration.MODE_CREATE)
                .setCodeLength(4)
                .setNewCodeValidation(true)
                .setUseFingerprint(false)
                .setTitle(getString(R.string.spin_plus_passcode_create))
                .setNewCodeValidationTitle(getString(R.string.spin_plus_passcode_confirm))
        fragment.setConfiguration(builder.build())
        fragment.setCodeCreateListener(
            object : PFLockScreenFragment.OnPFLockScreenCodeCreateListener {
                override fun onCodeCreated(encodedCode: String) {
                    SpinSupportUtils.setEncodedPasscode(context, encodedCode)
                    fragment.lifecycleScope.launch(Dispatchers.Main) {
                        Toast.makeText(
                            context,
                            context.getString(R.string.spin_plus_passcode_confirm_success),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        fragment.findNavController().popBackStack()
                    }
                }

                override fun onNewCodeValidationFailed() {
                    PFSecurityManager.getInstance().pinCodeHelper.delete(null)
                    SpinSupportUtils.setEncodedPasscode(context, null)
                    fragment.lifecycleScope.launch(Dispatchers.Main) {
                        Toast.makeText(
                            context,
                            context.getString(R.string.spin_plus_passcode_confirm_error),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            }
        )

        return fragment
    }

    private fun buildDisablePasscodeFragment(): Fragment {
        val context = requireContext()
        val fragment = PFLockScreenFragment()
        val builder: PFFLockScreenConfiguration.Builder =
            PFFLockScreenConfiguration.Builder(requireContext())
                .setMode(PFFLockScreenConfiguration.MODE_AUTH)
                .setUseFingerprint(false)
                .setTitle(getString(R.string.spin_plus_passcode_enter))
        fragment.setConfiguration(builder.build())
        fragment.setEncodedPinCode(SpinSupportUtils.getEncodedPasscode(requireContext()))
        fragment.setLoginListener(
            object : PFLockScreenFragment.OnPFLockScreenLoginListener {
                override fun onCodeInputSuccessful() {
                    PFSecurityManager.getInstance().pinCodeHelper.delete(null)
                    SpinSupportUtils.setEncodedPasscode(context, null)
                    dismiss()
                }

                override fun onPinLoginFailed() {
                    Toast.makeText(
                        requireContext(),
                        requireContext().getString(R.string.spin_plus_passcode_confirm_error),
                        Toast.LENGTH_SHORT
                    ).show()
                    dismiss()
                }

                override fun onFingerprintSuccessful() {
                    onCodeInputSuccessful()
                }

                override fun onFingerprintLoginFailed() {
                    onPinLoginFailed()
                }

                override fun onCancel() {
//                    dismiss()
                }

                private fun dismiss() {
                    fragment.lifecycleScope.launch(Dispatchers.Main) {
                        fragment.findNavController().popBackStack()
                    }
                }
            }
        )

        return fragment
    }
}
