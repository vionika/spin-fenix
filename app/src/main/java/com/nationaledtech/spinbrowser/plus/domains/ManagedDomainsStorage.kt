package com.nationaledtech.spinbrowser.plus.domains

import android.content.ContentValues
import android.content.Context
import android.net.Uri
import com.nationaledtech.spinbrowser.db.SpinDatabaseContract
import io.sentry.util.StringUtils

data class ManagedDomain(
    val guid: String,
    val name: String
)

interface ManagedDomainsStorage {
    fun getAllDomains(isAllowed: Boolean): List<ManagedDomain>
    fun getAllDomains(isAllowed: Boolean, blockedBySpinOnly: Boolean?): List<ManagedDomain>
    fun addDomain(domain: String, isAllowed: Boolean)
    fun addDomain(domain: String, isAllowed: Boolean, isBlockedBySpin: Boolean)
    fun deleteDomain(domain: String)
    fun clearStorage()
}

class DefaultManagedDomainsStorage(
    private val context: Context
) : ManagedDomainsStorage {
    private val MANAGED_DOMAINS_CONTENT_URL = "content://%s.db.browser/managed_domains"

    override fun getAllDomains(isAllowed: Boolean): List<ManagedDomain> {
        return getAllDomains(isAllowed, null)
    }

    override fun getAllDomains(isAllowed: Boolean, blockedBySpinOnly: Boolean?): List<ManagedDomain> {
        val policyContentUrl = MANAGED_DOMAINS_CONTENT_URL.format(context.packageName)
        var selection = "is_allowed = " + if (isAllowed) 1 else 0
        blockedBySpinOnly?.let {
            selection += " AND is_blocked_by_spin = " + if (blockedBySpinOnly) 1 else 0
        }

        val cursor = context.contentResolver.query(
            Uri.parse(policyContentUrl),
            arrayOf("_id", "name"),
            selection,
            null,
            null
        )

        cursor.use {
            if (it != null && it.moveToFirst()) {
                var result: MutableList<ManagedDomain> = mutableListOf()
                do {
                    val guid = it.getString(0)
                    val name = it.getString(1)
                    result.add(ManagedDomain(guid = guid ?: "", name = name ?: ""))
                } while (it.moveToNext())

                return result
            }
        }

        return listOf()
    }

    override fun addDomain(domain: String, isAllowed: Boolean) {
        addDomain(domain, isAllowed, false)
    }

    override fun addDomain(domain: String, isAllowed: Boolean, isBlockedBySpin: Boolean) {
        val contentValues = ContentValues(2).apply {
            put(SpinDatabaseContract.ManagedDomainsEntry.COLUMN_NAME, domain)
            put(SpinDatabaseContract.ManagedDomainsEntry.COLUMN_ALLOWED, isAllowed)
            put(SpinDatabaseContract.ManagedDomainsEntry.COLUMN_BLOCKED_BY_SPIN, if (isBlockedBySpin) 1 else 0)
        }

        val uri = SpinDatabaseContract.ManagedDomainsEntry.CONTENT_URI
        val selection = "${SpinDatabaseContract.ManagedDomainsEntry.COLUMN_NAME} = ?"
        val selectionArgs = arrayOf(domain)

        val cursor = context.contentResolver.query(
            uri,
            arrayOf(SpinDatabaseContract.ManagedDomainsEntry.COLUMN_NAME),
            selection,
            selectionArgs,
            null
        )

        if (cursor != null && cursor.moveToFirst()) {
            context.contentResolver.update(
                uri,
                contentValues,
                selection,
                selectionArgs
            )
        } else {
            context.contentResolver.insert(
                uri,
                contentValues
            )
        }

        cursor?.close()
    }

    override fun deleteDomain(domain: String) {
        val contentValues = ContentValues(1)
        contentValues.put(SpinDatabaseContract.ManagedDomainsEntry.COLUMN_NAME, domain)
        context.contentResolver.delete(
            SpinDatabaseContract.ManagedDomainsEntry.CONTENT_URI,
            "name=?",
            arrayOf(domain)
        )
    }

    override fun clearStorage() {
        context.contentResolver.delete(SpinDatabaseContract.ManagedDomainsEntry.CONTENT_URI, null, null);
    }
}

