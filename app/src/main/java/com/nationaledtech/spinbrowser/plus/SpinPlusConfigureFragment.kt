package com.nationaledtech.spinbrowser.plus

import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.preference.Preference
import androidx.preference.Preference.OnPreferenceClickListener
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment
import com.nationaledtech.spinbrowser.SpinSupportUtils
import com.nationaledtech.spinbrowser.plus.data.BackupFileNavigator
import com.nationaledtech.spinbrowser.plus.data.BackupStorage
import com.nationaledtech.spinbrowser.plus.data.SpinPlusDataManager
import com.nationaledtech.spinbrowser.plus.passcode.LockScreenFragmentBuilder
import com.nationaledtech.spinbrowser.ui.SpinProgressDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.mozilla.fenix.NavHostActivity
import org.mozilla.fenix.R
import org.mozilla.fenix.ext.getPreferenceKey


class SpinPlusConfigureFragment : PreferenceFragmentCompat() {
    private var isPopBackStack = false
    private val progressDialog: SpinProgressDialog? = null

    companion object {
        const val REQUEST_CODE_EXPORT_BACKUP = 101
        const val REQUEST_CODE_IMPORT_BACKUP = 102
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.spin_plus_configure_preferences, rootKey)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<Boolean>("isPopBackStack")
            ?.observe(
                viewLifecycleOwner,
            ) { result ->
                isPopBackStack = result
            }
    }

    override fun onResume() {
        super.onResume()
        setupPreferences()

        (activity as NavHostActivity).getSupportActionBarAndInflateIfNecessary().show()

        if (!isPopBackStack) {
            validatePasscodeIfNeeded()
        }
    }

    private fun setupPreferences() {
        val context = requireContext()

        val allowedDomainsList =
            requireNotNull(findPreference(getPreferenceKey(R.string.pref_key_spin_plus_allowed_list)))
        allowedDomainsList.onPreferenceClickListener = OnPreferenceClickListener {
            val directions =
                SpinPlusConfigureFragmentDirections.actionConfigureSpinToManagedDomains(isAllowed = true)
            Navigation.findNavController(requireView()).navigate(directions)
            true
        }

        val blockedDomainsList =
            requireNotNull(findPreference(getPreferenceKey(R.string.pref_key_spin_plus_blocked_list)))
        blockedDomainsList.onPreferenceClickListener = OnPreferenceClickListener {
            val directions =
                SpinPlusConfigureFragmentDirections.actionConfigureSpinToManagedDomains(isAllowed = false)
            Navigation.findNavController(requireView()).navigate(directions)
            true
        }

        val setPasscode =
            requireNotNull(findPreference(getPreferenceKey(R.string.pref_key_spin_plus_passcode)))
        if (SpinSupportUtils.isPasscodeSet(context)) {
            setPasscode.title = getString(R.string.spin_plus_passcode_disable)
        } else {
            setPasscode.title = getString(R.string.spin_plus_passcode_set)
        }
        setPasscode.onPreferenceClickListener = OnPreferenceClickListener {
            val directions = SpinPlusConfigureFragmentDirections.actionConfigureSpinToSetPasscode()
            Navigation.findNavController(requireView()).navigate(directions)
            true
        }

        val protectAddons =
            requireNotNull(findPreference<SwitchPreference>(getPreferenceKey(R.string.pref_key_protect_addons_with_passcode)))
        protectAddons.isChecked = SpinSupportUtils.shouldProtectAddonsWithPasscode(context)
        protectAddons.isEnabled = SpinSupportUtils.isPasscodeSet(context)
        protectAddons.onPreferenceChangeListener =
            Preference.OnPreferenceChangeListener { preference, newValue ->
                val isOn = newValue as Boolean
                if (isOn) {
                    SpinSupportUtils.setShouldProtectAddonsWithPasscode(context, true)
                    true
                } else {
                    validatePasscodeToDisableAddons()
                    false
                }
            }

        val backupPreference =
            requireNotNull(findPreference(getPreferenceKey(R.string.pref_key_spin_plus_data_backup)))
        backupPreference.onPreferenceClickListener = OnPreferenceClickListener {
            createBackup()
            true
        }

        val restorePreference =
            requireNotNull(findPreference(getPreferenceKey(R.string.pref_key_spin_plus_data_restore)))
        restorePreference.onPreferenceClickListener = OnPreferenceClickListener {
            displayConfirmRestoreBackupDialog()
            true
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE_IMPORT_BACKUP) {
            isPopBackStack = true
            data?.data?.let { uri ->
                val backupStorage = BackupStorage(requireContext())
                val data = backupStorage.retrieveBackupFromUri(uri)
                data?.let {
                    GlobalScope.launch(Dispatchers.IO) {
                        val backupManager = SpinPlusDataManager(requireContext())
                        backupManager.restoreFromBackup(data)

                        GlobalScope.launch(Dispatchers.Main) {
                            Toast.makeText(
                                context,
                                context?.getString(R.string.preferences_spin_plus_restore_success),
                                Toast.LENGTH_SHORT,
                            ).show()
                        }
                    }
                } ?: run {
                    Toast.makeText(
                        context,
                        context?.getString(R.string.preferences_spin_plus_restore_error),
                        Toast.LENGTH_SHORT,
                    ).show()
                }
            }
        } else if (requestCode == REQUEST_CODE_EXPORT_BACKUP) {
            isPopBackStack = true
            data?.data?.let { uri ->
                GlobalScope.launch(Dispatchers.IO) {
                    val backupStorage = BackupStorage(requireContext())
                    val backupManager = SpinPlusDataManager(requireContext())
                    val backupData = backupManager.createBackup()
                    var result = backupStorage.storeBackupToFile(requireContext(), backupData, uri)

                    GlobalScope.launch(Dispatchers.Main) {
                        val textId = if (result) R.string.preferences_spin_plus_backup_success else R.string.preferences_spin_plus_backup_error
                        Toast.makeText(
                            context,
                            context?.getString(textId),
                            Toast.LENGTH_SHORT,
                        ).show()
                    }
                }
            }
        }
    }

    private fun validatePasscodeIfNeeded() {
        val encodedPin = SpinSupportUtils.getEncodedPasscode(requireContext()) ?: return

        val fragment = LockScreenFragmentBuilder.build(requireContext(), encodedPin)
        fragment.setLoginListener(
            object : PFLockScreenFragment.OnPFLockScreenLoginListener {
                override fun onCodeInputSuccessful() {
                    activity?.supportFragmentManager?.beginTransaction()?.remove(fragment)?.commit()
                }

                override fun onPinLoginFailed() {
                    Toast.makeText(
                        requireContext(),
                        requireContext().getString(R.string.spin_plus_passcode_confirm_error),
                        Toast.LENGTH_SHORT,
                    ).show()
                    dismiss()
                }

                override fun onFingerprintSuccessful() {
                    onCodeInputSuccessful()
                }

                override fun onFingerprintLoginFailed() {
                    onPinLoginFailed()
                }

                override fun onCancel() {
//                    dismiss()
                }

                private fun dismiss() {
                    fragment.lifecycleScope.launch(Dispatchers.Main) {
                        fragment.findNavController().popBackStack()
                    }
                }
            },
        )

        activity?.supportFragmentManager
            ?.beginTransaction()
            ?.add(android.R.id.list_container, fragment, null)
            ?.addToBackStack(null)
            ?.commit()
    }

    private fun validatePasscodeToDisableAddons() {
        val encodedPin = SpinSupportUtils.getEncodedPasscode(requireContext()) ?: return

        val fragment = LockScreenFragmentBuilder.build(requireContext(), encodedPin)
        fragment.setLoginListener(
            object : PFLockScreenFragment.OnPFLockScreenLoginListener {
                override fun onCodeInputSuccessful() {
                    SpinSupportUtils.setShouldProtectAddonsWithPasscode(requireContext(), false)
                    setupPreferences()
                    dismiss()
                }

                override fun onPinLoginFailed() {
                    Toast.makeText(
                        requireContext(),
                        requireContext().getString(R.string.spin_plus_passcode_confirm_error),
                        Toast.LENGTH_SHORT,
                    ).show()
                    dismiss()
                }

                override fun onFingerprintSuccessful() {
                    onCodeInputSuccessful()
                }

                override fun onFingerprintLoginFailed() {
                    onPinLoginFailed()
                }

                override fun onCancel() {
//                    dismiss()
                }

                private fun dismiss() {
                    activity?.supportFragmentManager?.beginTransaction()?.remove(fragment)?.commit()
                }
            },
        )

        activity?.supportFragmentManager
            ?.beginTransaction()
            ?.add(android.R.id.list_container, fragment, null)
            ?.addToBackStack(null)
            ?.commit()
    }

    private fun displayConfirmRestoreBackupDialog() {
        activity?.let { activity ->
            AlertDialog.Builder(activity).apply {
                setMessage(R.string.preferences_spin_plus_restore_confirmation)
                setNegativeButton(R.string.bookmark_delete_negative) { dialog: DialogInterface, _ ->
                    dialog.cancel()
                }
                setPositiveButton(R.string.preferences_spin_plus_data_restore_title) { dialog: DialogInterface, _ ->
                    restoreBackup()
                    dialog.dismiss()
                }
                create()
            }.show()
        }
    }

    private fun createBackup() {
        BackupFileNavigator.getExportIntent()?.let { intent ->
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                requireActivity().startActivityFromFragment(
                    this,
                    intent,
                    REQUEST_CODE_EXPORT_BACKUP,
                )
            }
        }
//        GlobalScope.launch(Dispatchers.IO) {
//            val backupStorage = BackupStorage(requireContext())
//            val backupManager = SpinPlusDataManager(requireContext())
//            val backupData = backupManager.createBackup()
//            val backupUri = backupStorage.storeBackupToFileSystem(backupData)
//
//            GlobalScope.launch(Dispatchers.Main) {
//                backupUri?.let { uri ->
//                    BackupFileNavigator.getExportIntent(requireActivity())
//                }?.let { intent ->
//                    createBackupWithIntent(intent)
//                }
//            }
//        }
    }

    private fun createBackupWithIntent(intent: Intent) {
        if (intent != null) {
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                requireActivity().startActivityFromFragment(
                    this,
                    intent,
                    REQUEST_CODE_EXPORT_BACKUP,
                )
            }
        }
    }

    private fun restoreBackup() {
        BackupFileNavigator.getImportIntent()?.let { intent ->
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                requireActivity().startActivityFromFragment(
                    this,
                    intent,
                    REQUEST_CODE_IMPORT_BACKUP,
                )
            }
        }
    }
}
