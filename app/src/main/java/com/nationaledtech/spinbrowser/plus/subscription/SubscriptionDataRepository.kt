package com.nationaledtech.spinbrowser.plus.subscription

import com.android.billingclient.api.ProductDetails
import com.android.billingclient.api.Purchase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map


/**
 * The [SubscriptionDataRepository] processes and tranforms the [StateFlow] data received from
 * the [BillingClientWrapper] into [Flow] data available to the viewModel.
 *
 */
class SubscriptionDataRepository(billingClientWrapper: BillingClientWrapper) {

    // Set to true when a returned purchase is an auto-renewing basic subscription.
    val hasRenewableMonthly: Flow<Boolean> = billingClientWrapper.purchases.map { purchaseList ->
        purchaseList.any { purchase ->
            purchase.products.contains(MONTHLY_SUB) && purchase.isAutoRenewing
        }
    }

    val hasRenewableYearly: Flow<Boolean> = billingClientWrapper.purchases.map { purchaseList ->
        purchaseList.any { purchase ->
            purchase.products.contains(YEARLY_SUB) && purchase.isAutoRenewing
        }
    }

    // Set to true when a returned purchase is prepaid basic subscription.
    val hasPrepaidMonthly: Flow<Boolean> = billingClientWrapper.purchases.map { purchaseList ->
        purchaseList.any { purchase ->
            !purchase.isAutoRenewing && purchase.products.contains(MONTHLY_SUB)
        }
    }

    val hasPrepaidYearly: Flow<Boolean> = billingClientWrapper.purchases.map { purchaseList ->
        purchaseList.any { purchase ->
            !purchase.isAutoRenewing && purchase.products.contains(YEARLY_SUB)
        }
    }

    // ProductDetails for the basic subscription.
    val monthlyProductDetails: Flow<ProductDetails> =
        billingClientWrapper.productWithProductDetails.filter {
            it.containsKey(
                MONTHLY_SUB
            )
        }.map { it[MONTHLY_SUB]!! }

    val yearlyProductDetails: Flow<ProductDetails> =
        billingClientWrapper.productWithProductDetails.filter {
            it.containsKey(
                YEARLY_SUB
            )
        }.map { it[YEARLY_SUB]!! }

    companion object {
        private const val MONTHLY_SUB = "spin_monthly"
        private const val YEARLY_SUB = "spinplus_yearly"
    }
}
