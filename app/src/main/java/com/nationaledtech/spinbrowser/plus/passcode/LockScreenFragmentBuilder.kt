package com.nationaledtech.spinbrowser.plus.passcode

import android.app.AlertDialog
import android.content.Context
import com.beautycoder.pflockscreen.PFFLockScreenConfiguration
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment
import org.mozilla.fenix.R

class LockScreenFragmentBuilder {
    companion object {
        fun build(
            context: Context,
            encodedPin: String
        ): PFLockScreenFragment {
            val fragment = PFLockScreenFragment()
            val builder: PFFLockScreenConfiguration.Builder =
                PFFLockScreenConfiguration.Builder(context)
                    .setMode(PFFLockScreenConfiguration.MODE_AUTH)
                    .setUseFingerprint(false)
                    .setTitle(context.getString(R.string.spin_plus_passcode_enter))
                    .setLeftButton(context.getString(R.string.spin_plus_passcode_forgot_title))
            fragment.setConfiguration(builder.build())
            fragment.setEncodedPinCode(encodedPin)

            fragment.setOnLeftButtonClickListener {
                val alertDialog: AlertDialog = AlertDialog.Builder(context).create()
                alertDialog.setMessage(context.getString(R.string.spin_plus_passcode_forgot_alert_message))
                alertDialog.setButton(
                    AlertDialog.BUTTON_NEUTRAL,
                    context.getString(R.string.spin_plus_passcode_forgot_alert_button_title)
                ) { dialog, _ -> dialog.dismiss() }
                alertDialog.show()
            }

            return fragment
        }
    }
}
