package com.nationaledtech.spinbrowser.plus.data

import android.content.Context
import com.nationaledtech.spinbrowser.plus.data.model.BackupBookmarkNode
import com.nationaledtech.spinbrowser.plus.data.model.BackupBookmarkNodeType
import com.nationaledtech.spinbrowser.plus.data.model.BackupData
import com.nationaledtech.spinbrowser.plus.data.model.BackupManagedDomain
import com.nationaledtech.spinbrowser.plus.domains.DefaultManagedDomainsStorage
import com.nationaledtech.spinbrowser.plus.domains.ManagedDomain
import mozilla.appservices.places.BookmarkRoot
import mozilla.components.concept.storage.BookmarkNode
import mozilla.components.concept.storage.BookmarkNodeType
import org.mozilla.fenix.ext.components

class SpinPlusDataManager(
    context: Context,
) {
    private val managedDomainsStorage = DefaultManagedDomainsStorage(context)
    private val bookmarksStorage = context.components.core.bookmarksStorage

    suspend fun createBackup(): BackupData {
        val allowedList = managedDomainsStorage.getAllDomains(true).map {
            BackupManagedDomain(it.name)
        }

        val blockedList = managedDomainsStorage.getAllDomains(false).map {
            BackupManagedDomain(it.name)
        }

        val bookmarks: MutableList<BackupBookmarkNode> = mutableListOf()
        BookmarkRoot.values().forEach { it ->
            when (it) {
                BookmarkRoot.Root -> null
                else -> {
                    bookmarksStorage?.getTree(
                        it.id,
                        recursive = true,
                    )?.let { item ->
                        bookmarks += item.asBackupBookmarkNode()
                    }
                }
            }
        }

        return BackupData(
            allowedList = allowedList,
            blockedList = blockedList,
            bookmarks = bookmarks,
        )
    }

    suspend fun restoreFromBackup(backupData: BackupData) {
        managedDomainsStorage.clearStorage()

        val allowedList = backupData.allowedList.map {
            ManagedDomain("", it.name)
        }
        for (domain in allowedList) {
            managedDomainsStorage.addDomain(domain.name,true)
        }

        val blockedList = backupData.blockedList.map {
            ManagedDomain("", it.name)
        }
        for (domain in blockedList) {
            managedDomainsStorage.addDomain(domain.name,false)
        }

        BookmarkRoot.values().forEach {
            when (it) {
                BookmarkRoot.Root -> null
                else -> bookmarksStorage.getTree(
                    it.id,
                    recursive = true,
                )?.children?.forEach { node ->
                    bookmarksStorage.deleteNode(node.guid)
                }
            }
        }

        backupData.bookmarks?.forEach {
            restoreBookmark(it, it.parentGuid ?: "")
        }
    }

    private suspend fun restoreBookmark(bookmark: BackupBookmarkNode, parentGuid: String) {
        getBackupBookmarkNodeType(bookmark)?.let { nodeType ->
            when (nodeType) {
                BackupBookmarkNodeType.ITEM -> restoreItemBookmark(bookmark, parentGuid)
                BackupBookmarkNodeType.SEPARATOR -> restoreSeparatorBookmark(bookmark, parentGuid)
                BackupBookmarkNodeType.FOLDER -> {
                    val guid = restoreFolderBookmark(bookmark, parentGuid)
                    bookmark.children?.let { bookmarks ->
                        for (item in bookmarks) {
                            restoreBookmark(item, guid)
                        }
                    }
                }
            }
        }
    }

    private fun getBackupBookmarkNodeType(bookmark: BackupBookmarkNode): BackupBookmarkNodeType? {
        if (bookmark.type != null) {
            return bookmark.type
        }

        if (!bookmark.url.isNullOrEmpty()) {
            return  BackupBookmarkNodeType.ITEM
        }

        if (!bookmark.title.isNullOrEmpty()) {
            return BackupBookmarkNodeType.FOLDER
        }

        return BackupBookmarkNodeType.SEPARATOR
    }

    private suspend fun restoreItemBookmark(bookmark: BackupBookmarkNode, parentGuid: String) {
        val url = bookmark.url
        val title = bookmark.title
        val position = bookmark.position

        if (url != null && title != null && position != null) {
            bookmarksStorage.addItem(parentGuid, url, title, position)
        }
    }

    private suspend fun restoreFolderBookmark(bookmark: BackupBookmarkNode, parentGuid: String): String {
        val title = bookmark.title
        val position = bookmark.position

        var guid = ""
        if (title != null && position != null) {
            guid = bookmarksStorage.addFolder(parentGuid, title, position)
        }

        return guid.ifEmpty { bookmark.guid }
    }

    private suspend fun restoreSeparatorBookmark(bookmark: BackupBookmarkNode, parentGuid: String) {
        val position = bookmark.position

        if (position != null) {
            bookmarksStorage.addSeparator(parentGuid, position)
        }
    }
}

internal fun BookmarkNodeType.asBackupBookmarkNodeType(): BackupBookmarkNodeType = when (this) {
    BookmarkNodeType.FOLDER -> BackupBookmarkNodeType.FOLDER
    BookmarkNodeType.SEPARATOR -> BackupBookmarkNodeType.SEPARATOR
    BookmarkNodeType.ITEM -> BackupBookmarkNodeType.ITEM
}

internal fun BookmarkNode.asBackupBookmarkNode(): BackupBookmarkNode {
    return BackupBookmarkNode(
        this.type.asBackupBookmarkNodeType(),
        this.guid,
        this.parentGuid,
        this.position,
        this.title,
        this.url,
        this.dateAdded,
        this.children?.map { it.asBackupBookmarkNode() },
    )
}
