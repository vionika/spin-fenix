package com.nationaledtech.spinbrowser.plus.domains.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.nationaledtech.spinbrowser.plus.domains.ManagedDomain
import com.nationaledtech.spinbrowser.plus.domains.interactor.ManagedDomainsInteractor

class ManagedDomainsAdapter(
    private val interactor: ManagedDomainsInteractor
) : ListAdapter<ManagedDomain, ManagedDomainItemViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ManagedDomainItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(ManagedDomainItemViewHolder.LAYOUT_ID, parent, false)
        return ManagedDomainItemViewHolder(view, interactor)
    }

    override fun onBindViewHolder(holder: ManagedDomainItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    internal object DiffCallback : DiffUtil.ItemCallback<ManagedDomain>() {
        override fun areItemsTheSame(oldItem: ManagedDomain, newItem: ManagedDomain) =
            oldItem.guid == newItem.guid

        override fun areContentsTheSame(oldItem: ManagedDomain, newItem: ManagedDomain) =
            oldItem == newItem
    }
}
