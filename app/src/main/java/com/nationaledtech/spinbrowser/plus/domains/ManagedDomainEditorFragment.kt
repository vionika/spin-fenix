package com.nationaledtech.spinbrowser.plus.domains

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.nationaledtech.spinbrowser.plus.domains.controller.DefaultManagedDomainEditorController
import com.nationaledtech.spinbrowser.plus.domains.interactor.DefaultManagedDomainEditorInteractor
import com.nationaledtech.spinbrowser.plus.domains.interactor.ManagedDomainEditorInteractor
import com.nationaledtech.spinbrowser.plus.domains.view.ManagedDomainEditorView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mozilla.components.support.ktx.android.view.hideKeyboard
import mozilla.components.support.ktx.android.view.showKeyboard
import org.mozilla.fenix.NavHostActivity
import org.mozilla.fenix.R
import org.mozilla.fenix.SecureFragment
import org.mozilla.fenix.databinding.FragmentDomainEditorBinding
import org.mozilla.fenix.ext.placeCursorAtEnd
import org.mozilla.fenix.ext.showToolbar

class ManagedDomainEditorFragment : Fragment(R.layout.fragment_domain_editor) {
    private lateinit var domainEditorState: ManagedDomainEditorState
    private lateinit var domainEditorView: ManagedDomainEditorView

    private val args by navArgs<ManagedDomainEditorFragmentArgs>()

    private lateinit var interactor: ManagedDomainEditorInteractor

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val storage = DefaultManagedDomainsStorage(context = requireContext())
        interactor = DefaultManagedDomainEditorInteractor(
            controller = DefaultManagedDomainEditorController(
                isAllowed = args.isAllowed,
                storage = storage,
                lifecycleScope = lifecycleScope,
                navController = findNavController()
            )
        )

        val binding = FragmentDomainEditorBinding.bind(view)

        lifecycleScope.launch(Dispatchers.Main) {
            domainEditorState = withContext(Dispatchers.IO) {
                ManagedDomainEditorState(
                    isAllowed = args.isAllowed
                )
            }
            domainEditorView = ManagedDomainEditorView(binding, interactor, args.isAllowed)
            domainEditorView.bind()

            binding.domainInput.apply {
                requestFocus()
                placeCursorAtEnd()
                showKeyboard()
            }
        }
    }

    override fun onResume() {
        super.onResume()

        (activity as NavHostActivity).getSupportActionBarAndInflateIfNecessary().show()

        if (args.isAllowed) {
            showToolbar(getString(R.string.spin_plus_domains_add_allowed_domain))
        } else {
            showToolbar(getString(R.string.spin_plus_domains_add_blocked_domain))
        }
    }

    override fun onPause() {
        view?.hideKeyboard()

        super.onPause()
    }
}
