package com.nationaledtech.spinbrowser.plus.domains.view

import android.content.Context
import android.content.res.ColorStateList
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import com.google.android.material.textfield.TextInputEditText
import com.nationaledtech.spinbrowser.plus.domains.ManagedDomainEditorState
import com.nationaledtech.spinbrowser.plus.domains.interactor.ManagedDomainEditorInteractor
import com.nationaledtech.spinbrowser.services.ExamineDomainService
import mozilla.components.support.ktx.android.content.getColorFromAttr
import mozilla.components.support.ktx.android.view.hideKeyboard
import org.mozilla.fenix.R
import org.mozilla.fenix.databinding.FragmentDomainEditorBinding
import java.net.IDN
import java.net.URI

class ManagedDomainEditorView(
    private val binding: FragmentDomainEditorBinding,
    private val interactor: ManagedDomainEditorInteractor,
    private val validateDomainRequired: Boolean = false
) {
    private val examineService = ExamineDomainService()
    private val defaultProhibitedCategories = mapOf(
        1011 to "Pornography",
        1058 to "VPN or Proxy Sites",
        1062 to "Nudity"
    )

    fun bind() {
        binding.cancelButton.setOnClickListener {
            interactor.onCancelButtonClicked()
        }

        binding.saveButton.setOnClickListener {
            saveDomain()
        }

        binding.domainLayout.setErrorTextColor(
            ColorStateList.valueOf(
                binding.root.context.getColorFromAttr(R.attr.textWarning)
            )
        )

        binding.domainInput.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                saveDomain()
                true
            } else {
                false
            }
        }
    }

    private fun saveDomain() {
        binding.root.hideKeyboard()

        val fixedDomain = fixDomainName(binding.domainInput.text.toString())
        if (fixedDomain == null) {
            binding.domainLayout.error =
                binding.root.context.getString(R.string.spin_plus_domains_error_invalid_domain)
            binding.domainTitle.setTextColor(binding.root.context.getColorFromAttr(R.attr.textWarning))
            binding.domainInput.requestKeyboardFocus()
            return
        }

        if (validateDomainRequired) {
            binding.domainInput.isEnabled = false

            val asciiDomainName = try {
                IDN.toASCII(fixedDomain)
            } catch (exception: Exception) {
                fixedDomain
            }
            examineService.examineDomain(asciiDomainName) {
                val categoriesList = it.toCollection(ArrayList())
                if (categoriesList.isEmpty()) {
                    binding.domainLayout.error =
                        binding.root.context.getString(R.string.spin_plus_domains_error_network_unavailable)
                    binding.domainTitle.setTextColor(binding.root.context.getColorFromAttr(R.attr.textWarning))
                    binding.domainInput.requestKeyboardFocus()
                    return@examineDomain
                }
                categoriesList.retainAll(defaultProhibitedCategories.keys)
                if (categoriesList.size > 0) {
                    binding.domainLayout.error =
                        binding.root.context.getString(
                            R.string.spin_plus_domains_error_restricted_domain,
                            defaultProhibitedCategories[categoriesList.first()]
                        )
                    binding.domainTitle.setTextColor(binding.root.context.getColorFromAttr(R.attr.textWarning))
                    binding.domainInput.requestKeyboardFocus()
                } else {
                    interactor.onSaveDomain(fixedDomain)
                }
                binding.domainInput.isEnabled = true
            }
        } else {
            interactor.onSaveDomain(fixedDomain)
        }
    }

    private fun fixDomainName(domain: String): String? {
        return try {
            var fixedDomain = if (domain.startsWith("www.")) domain.substring(4) else domain
            fixedDomain =
                if (fixedDomain.startsWith("https://")) fixedDomain else "https://$fixedDomain"
            val uri = URI(fixedDomain)
            var domainName: String = uri.toURL().host
            if (domainName.contains(".")) domainName else null
        } catch (exception: Exception) {
            null
        }
    }
}

fun TextInputEditText.requestKeyboardFocus() {
    val inputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}