package com.nationaledtech.spinbrowser.plus.subscription.flow

import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.android.billingclient.api.BillingFlowParams
import com.android.billingclient.api.ProductDetails
import com.android.billingclient.api.Purchase
import com.nationaledtech.spinbrowser.SpinSupportUtils
import com.nationaledtech.spinbrowser.plus.promo.SpinPlusPromoFragmentDirections
import com.nationaledtech.spinbrowser.plus.subscription.BillingClientWrapper
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import org.mozilla.fenix.FenixApplication
import org.mozilla.fenix.NavHostActivity
import org.mozilla.fenix.R
import org.mozilla.fenix.databinding.FragmentBuySubscriptionBinding
import org.mozilla.fenix.settings.SettingsFragmentDirections

enum class SubscriptionType {
    MONTH, YEAR
}

class BuySubscriptionFragment : Fragment(R.layout.fragment_buy_subscription) {
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    private var monthlyProductDetails: ProductDetails? = null
    private var yearlyProductDetails: ProductDetails? = null
    private var selectedProductDetails: ProductDetails? = null

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }
    override fun onDestroy() {
        super.onDestroy()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
    }
    override fun onResume() {
        super.onResume()

        (activity as NavHostActivity).getSupportActionBarAndInflateIfNecessary().show()

        Handler().postDelayed({
            validateProducts()
        }, 5000)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentBuySubscriptionBinding.bind(view)

        binding.subscriptionsLinearLayout.visibility = View.INVISIBLE
        binding.subscriptionsProgressBar.visibility = View.VISIBLE
        binding.buyButton.alpha = 0.5F
        binding.buyButton.isEnabled = false

        val manager = (activity?.application as? FenixApplication)?.subscriptionManager
        manager?.setup()

        lifecycleScope.launch(ioDispatcher) {
            manager?.productsForSaleFlows?.collectLatest {
                monthlyProductDetails = it.monthlyProductDetails
                val monthlyFormattedPrice = monthlyProductDetails?.subscriptionOfferDetails?.first()?.pricingPhases?.pricingPhaseList?.last()?.formattedPrice

                yearlyProductDetails = it.yearlyProductDetails
                val yearlyFormattedPrice = yearlyProductDetails?.subscriptionOfferDetails?.first()?.pricingPhases?.pricingPhaseList?.last()?.formattedPrice

                lifecycleScope.launch(Main) {
                    binding.monthlyDescriptionTextView.text = getString(
                        R.string.spin_plus_subscription_monthly_subtitle,
                        monthlyFormattedPrice
                    )
                    binding.yearlyDescriptionTextView.text = getString(
                        R.string.spin_plus_subscription_yearly_subtitle,
                        yearlyFormattedPrice
                    )

                    selectSubscription(SubscriptionType.MONTH, binding)

                    binding.monthlySubscriptionLayout.setOnClickListener {
                        selectSubscription(SubscriptionType.MONTH, binding)
                    }
                    binding.monthlySubscriptionRadio.setOnClickListener {
                        selectSubscription(SubscriptionType.MONTH, binding)
                    }

                    binding.yearlySubscriptionLayout.setOnClickListener {
                        selectSubscription(SubscriptionType.YEAR, binding)
                    }
                    binding.yearlySubscriptionRadio.setOnClickListener {
                        selectSubscription(SubscriptionType.YEAR, binding)
                    }

                    if (monthlyProductDetails != null && yearlyProductDetails != null) {
                        binding.subscriptionsLinearLayout.visibility = View.VISIBLE
                        binding.subscriptionsProgressBar.visibility = View.GONE
                        binding.buyButton.alpha = 1.0F
                        binding.buyButton.isEnabled = true
                    }
                }

                manager?.userCurrentSubscriptionFlow?.collectLatest {
                    if (it.hasSubscription) {
                        lifecycleScope.launch(Main) {
                            // Reset passcode on the new subscription
                            SpinSupportUtils.setEncodedPasscode(requireContext(), null)

                            Toast.makeText(
                                requireContext(),
                                requireContext().getString(R.string.spin_plus_subscription_success_message),
                                Toast.LENGTH_SHORT
                            ).show()
                            findNavController().navigateUp()
                            findNavController().navigate(SettingsFragmentDirections.actionSettingsFragmentToConfigureSpinFragment())
                        }
                    }
                }
            }
        }

        binding.buyButton.setOnClickListener {
            lifecycleScope.launch(ioDispatcher) {
                val selectedDetails = selectedProductDetails
                val billingClient = manager?.billingClient
                val activity = activity
                if (selectedDetails != null && billingClient != null && activity != null) {
                    buy(billingClient, selectedDetails, null, activity, "")
                }
            }
        }
    }

    fun buy(
        billingClient: BillingClientWrapper,
        productDetails: ProductDetails,
        currentPurchases: List<Purchase>?,
        activity: Activity,
        tag: String
    ) {
        val offers =
            productDetails.subscriptionOfferDetails?.let {
                retrieveEligibleOffers(
                    offerDetails = it,
                    tag = tag.lowercase()
                )
            }
        val offerToken = offers?.let { leastPricedOfferToken(it) }
        val oldPurchaseToken: String

        // Get current purchase. In this app, a user can only have one current purchase at
        // any given time.
        if (!currentPurchases.isNullOrEmpty() &&
            currentPurchases.size == MAX_CURRENT_PURCHASES_ALLOWED
        ) {
            // This either an upgrade, downgrade, or conversion purchase.
            val currentPurchase = currentPurchases.first()

            // Get the token from current purchase.
            oldPurchaseToken = currentPurchase.purchaseToken

            val billingParams = offerToken?.let {
                upDowngradeBillingFlowParamsBuilder(
                    productDetails = productDetails,
                    offerToken = it,
                    oldToken = oldPurchaseToken
                )
            }

            if (billingParams != null) {
                billingClient.launchBillingFlow(
                    activity,
                    billingParams
                )
            }
        } else if (currentPurchases == null) {
            // This is a normal purchase.
            val billingParams = offerToken?.let {
                billingFlowParamsBuilder(
                    productDetails = productDetails,
                    offerToken = it
                )
            }

            if (billingParams != null) {
                billingClient.launchBillingFlow(
                    activity,
                    billingParams.build()
                )
            }
        } else if (!currentPurchases.isNullOrEmpty() &&
            currentPurchases.size > MAX_CURRENT_PURCHASES_ALLOWED
        ) {
            // The developer has allowed users  to have more than 1 purchase, so they need to
            /// implement a logic to find which one to use.
            Log.d(TAG, "User has more than 1 current purchase.")
        }
    }

    private fun validateProducts() {
        if (!isAdded) {
            return
        }

        if (monthlyProductDetails == null || yearlyProductDetails == null) {
            Toast.makeText(
                requireContext(),
                requireContext().getString(R.string.spin_plus_error_billing_unavailable),
                Toast.LENGTH_SHORT
            ).show()
            findNavController().popBackStack()
        }
    }

    private fun billingFlowParamsBuilder(
        productDetails: ProductDetails,
        offerToken: String
    ): BillingFlowParams.Builder {
        return BillingFlowParams.newBuilder().setProductDetailsParamsList(
            listOf(
                BillingFlowParams.ProductDetailsParams.newBuilder()
                    .setProductDetails(productDetails)
                    .setOfferToken(offerToken)
                    .build()
            )
        )
    }

    private fun retrieveEligibleOffers(
        offerDetails: MutableList<ProductDetails.SubscriptionOfferDetails>,
        tag: String
    ): List<ProductDetails.SubscriptionOfferDetails> {
        val eligibleOffers = emptyList<ProductDetails.SubscriptionOfferDetails>().toMutableList()
        offerDetails.forEach { offerDetail ->
            if (offerDetail.offerTags.contains(tag)) {
                eligibleOffers.add(offerDetail)
            }
        }

        return eligibleOffers
    }

    private fun leastPricedOfferToken(
        offerDetails: List<ProductDetails.SubscriptionOfferDetails>
    ): String {
        var offerToken = String()
        var leastPricedOffer: ProductDetails.SubscriptionOfferDetails
        var lowestPrice = Int.MAX_VALUE

        if (!offerDetails.isNullOrEmpty()) {
            for (offer in offerDetails) {
                for (price in offer.pricingPhases.pricingPhaseList) {
                    if (price.priceAmountMicros < lowestPrice) {
                        lowestPrice = price.priceAmountMicros.toInt()
                        leastPricedOffer = offer
                        offerToken = leastPricedOffer.offerToken
                    }
                }
            }
        }
        return offerToken
    }

    private fun upDowngradeBillingFlowParamsBuilder(
        productDetails: ProductDetails,
        offerToken: String,
        oldToken: String
    ): BillingFlowParams {
        return BillingFlowParams.newBuilder().setProductDetailsParamsList(
            listOf(
                BillingFlowParams.ProductDetailsParams.newBuilder()
                    .setProductDetails(productDetails)
                    .setOfferToken(offerToken)
                    .build()
            )
        ).setSubscriptionUpdateParams(
            BillingFlowParams.SubscriptionUpdateParams.newBuilder()
                .setOldPurchaseToken(oldToken)
                .setReplaceProrationMode(
                    BillingFlowParams.ProrationMode.IMMEDIATE_AND_CHARGE_FULL_PRICE
                )
                .build()
        ).build()
    }

    private fun selectSubscription(type: SubscriptionType, binding: FragmentBuySubscriptionBinding) {
        when (type) {
            SubscriptionType.MONTH -> {
                binding.monthlySubscriptionRadio.isChecked = true
                binding.yearlySubscriptionRadio.isChecked = false
                selectedProductDetails = monthlyProductDetails
            }
            SubscriptionType.YEAR -> {
                binding.yearlySubscriptionRadio.isChecked = true
                binding.monthlySubscriptionRadio.isChecked = false
                selectedProductDetails = yearlyProductDetails
            }
        }
    }

    companion object {
        private const val TAG: String = "BuySubscriptionFragment"

        private const val MAX_CURRENT_PURCHASES_ALLOWED = 1
    }
}
