package com.nationaledtech.spinbrowser.plus.data

import android.content.Context
import android.content.Intent
import android.net.Uri
import java.text.SimpleDateFormat
import java.util.*

object BackupFileNavigator {
    fun getImportIntent(): Intent {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "*/*"
        return intent
    }

    fun getExportIntent(): Intent {
        val intent = Intent()
        intent.action = Intent.ACTION_CREATE_DOCUMENT
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "application/octet-stream"
        intent.putExtra(Intent.EXTRA_TITLE, generateBackupFileName())
        return intent
    }

    private fun generateBackupFileName(): String? {
        return SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
            .format(Date()) + "." + "spin"
    }
}
