package com.nationaledtech.spinbrowser.plus.domains.interactor

import com.nationaledtech.spinbrowser.plus.domains.controller.ManagedDomainEditorController

interface ManagedDomainEditorInteractor {
    fun onCancelButtonClicked()
    fun onSaveDomain(domain: String)
}

class DefaultManagedDomainEditorInteractor(
    private val controller: ManagedDomainEditorController
) : ManagedDomainEditorInteractor {

    override fun onCancelButtonClicked() {
        controller.handleCancelButtonClicked()
    }

    override fun onSaveDomain(domain: String) {
        controller.handleSaveDomain(domain)
    }
}
