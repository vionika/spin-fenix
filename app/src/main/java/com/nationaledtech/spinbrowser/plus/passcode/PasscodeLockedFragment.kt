package com.nationaledtech.spinbrowser.plus.passcode

import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment
import com.nationaledtech.spinbrowser.SpinSupportUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.mozilla.fenix.FenixApplication
import org.mozilla.fenix.R
import org.mozilla.fenix.ext.application

open class PasscodeLockedFragment : Fragment() {
    fun validatePasscodeIfNeeded() {
        runBlocking(Dispatchers.IO) {
            val manager = (context?.application as? FenixApplication)?.subscriptionManager
            val hasSubscription = manager?.userCurrentSubscriptionFlow?.first()?.hasSubscription
            val encodedPin = SpinSupportUtils.getEncodedPasscode(requireContext())
            if (hasSubscription == true && encodedPin != null) {
                val fragment = LockScreenFragmentBuilder.build(requireContext(), encodedPin)
                fragment.setLoginListener(
                    object : PFLockScreenFragment.OnPFLockScreenLoginListener {
                        override fun onCodeInputSuccessful() {
                            dismiss()
                        }

                        override fun onPinLoginFailed() {
                            Toast.makeText(
                                requireContext(),
                                requireContext().getString(R.string.spin_plus_passcode_confirm_error),
                                Toast.LENGTH_SHORT
                            ).show()

                            fragment.lifecycleScope.launch(Dispatchers.Main) {
                                fragment.findNavController().popBackStack()
                            }
                        }

                        override fun onFingerprintSuccessful() {
                            onCodeInputSuccessful()
                        }

                        override fun onFingerprintLoginFailed() {
                            onPinLoginFailed()
                        }

                        override fun onCancel() {
                            dismiss()
                        }

                        private fun dismiss() {
                            activity?.supportFragmentManager?.beginTransaction()?.remove(fragment)?.commit()
                        }
                    }
                )

                activity?.supportFragmentManager
                    ?.beginTransaction()
                    ?.add(R.id.passcode_container, fragment, "fragmentId")
                    ?.commit()
            }
        }
    }
}
