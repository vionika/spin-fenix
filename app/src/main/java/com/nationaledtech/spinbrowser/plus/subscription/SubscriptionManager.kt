package com.nationaledtech.spinbrowser.plus.subscription

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.first

class SubscriptionManager(
    private val context: Context
) {
    var billingClient: BillingClientWrapper = BillingClientWrapper(context)
    private var repo: SubscriptionDataRepository =
        SubscriptionDataRepository(billingClientWrapper = billingClient)
    private val _billingConnectionState = MutableLiveData(false)
    val billingConnectionState: LiveData<Boolean> = _billingConnectionState

    open fun setup() {
        if (billingClient.isReady()) {
            billingClient.queryProductDetails()
            billingClient.queryPurchases()
            return
        }

        billingClient.startBillingConnection(billingConnectionState = _billingConnectionState)
    }

    // The productsForSaleFlows object combines all the Product flows into one for emission.
    val productsForSaleFlows = combine(
        repo.monthlyProductDetails,
        repo.yearlyProductDetails
    ) { monthlyProductDetails, yearlyProductDetails
        ->
        SubscriptionState(monthlyProductDetails = monthlyProductDetails, yearlyProductDetails = yearlyProductDetails)
    }

    // The userCurrentSubscriptionFlow object combines all the possible subscription flows into one
    // for emission.
    val userCurrentSubscriptionFlow = combine(
        repo.hasRenewableMonthly,
        repo.hasPrepaidMonthly,
        repo.hasRenewableYearly,
        repo.hasPrepaidYearly
    ) { hasRenewableMonthly,
        hasPrepaidMonthly,
        hasRenewableYearly,
        hasPrepaidYearly
        ->
        SubscriptionState(
            hasRenewableMonthly = hasRenewableMonthly,
            hasPrepaidMonthly = hasPrepaidMonthly,
            hasRenewableYearly = hasRenewableYearly,
            hasPrepaidYearly = hasPrepaidYearly,
            hasSubscription = (hasRenewableMonthly || hasPrepaidMonthly || hasRenewableYearly || hasPrepaidYearly)
        )
    }
}
