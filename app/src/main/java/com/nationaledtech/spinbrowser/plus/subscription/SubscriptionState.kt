package com.nationaledtech.spinbrowser.plus.subscription

import com.android.billingclient.api.ProductDetails
import com.android.billingclient.api.Purchase

data class SubscriptionState(
    val hasRenewableMonthly: Boolean? = false,
    val hasPrepaidMonthly: Boolean? = false,
    val monthlyProductDetails: ProductDetails? = null,
    val hasRenewableYearly: Boolean? = false,
    val hasPrepaidYearly: Boolean? = false,
    val yearlyProductDetails: ProductDetails? = null,
    val purchases: List<Purchase>? = null,
    val hasSubscription: Boolean = false
)
