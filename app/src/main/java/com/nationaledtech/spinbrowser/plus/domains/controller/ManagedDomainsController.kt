package com.nationaledtech.spinbrowser.plus.domains.controller

import androidx.navigation.NavController
import com.nationaledtech.spinbrowser.plus.domains.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

interface ManagedDomainsController {
    fun handleAddDomainClicked()
    fun handleDeleteDomainClicked(domain: ManagedDomain)
}

class DefaultManagedDomainsController(
    private val isAllowed: Boolean,
    private val store: ManagedDomainsStore,
    private val storage: ManagedDomainsStorage,
    private val lifecycleScope: CoroutineScope,
    private val navController: NavController,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ManagedDomainsController {
    override fun handleAddDomainClicked() {
        navigateToDomainEditor()
    }

    override fun handleDeleteDomainClicked(domain: ManagedDomain) {
        lifecycleScope.launch(ioDispatcher) {
            storage.deleteDomain(domain.name)

            lifecycleScope.launch(Dispatchers.IO) {
                val domains = storage.getAllDomains(isAllowed)
                lifecycleScope.launch(Dispatchers.Main) {
                    store.dispatch(ManagedDomainsAction.UpdateDomains(domains))
                }
            }
        }
    }

    private fun navigateToDomainEditor() {
        navController.navigate(
            ManagedDomainsFragmentDirections
                .actionDomainsFragmentToDomainEditorFragment(
                    isAllowed = isAllowed
                )
        )
    }
}
