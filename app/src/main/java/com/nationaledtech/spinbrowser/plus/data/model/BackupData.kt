package com.nationaledtech.spinbrowser.plus.data.model

data class BackupData(
    val allowedList: List<BackupManagedDomain>,
    val blockedList: List<BackupManagedDomain>,
    val bookmarks: List<BackupBookmarkNode>?,
)

data class BackupManagedDomain(
    val name: String
)

data class BackupBookmarkNode(
    val type: BackupBookmarkNodeType,
    val guid: String,
    val parentGuid: String?,
    val position: UInt?,
    val title: String?,
    val url: String?,
    val dateAdded: Long,
    val children: List<BackupBookmarkNode>?,
)

enum class BackupBookmarkNodeType {
    ITEM, FOLDER, SEPARATOR
}
