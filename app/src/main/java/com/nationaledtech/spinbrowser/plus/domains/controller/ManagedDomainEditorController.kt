package com.nationaledtech.spinbrowser.plus.domains.controller

import androidx.navigation.NavController
import com.nationaledtech.spinbrowser.plus.domains.ManagedDomainsStorage
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

interface ManagedDomainEditorController {
    fun handleCancelButtonClicked()
    fun handleSaveDomain(domain: String)
}

class DefaultManagedDomainEditorController(
    private val isAllowed: Boolean,
    private val storage: ManagedDomainsStorage,
    private val lifecycleScope: CoroutineScope,
    private val navController: NavController,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ManagedDomainEditorController {

    override fun handleCancelButtonClicked() {
        navController.popBackStack()
    }

    override fun handleSaveDomain(domain: String) {
        lifecycleScope.launch(ioDispatcher) {
            storage.addDomain(domain = domain, isAllowed = isAllowed)

            lifecycleScope.launch(Dispatchers.Main) {
                navController.popBackStack()
            }
        }
    }
}
