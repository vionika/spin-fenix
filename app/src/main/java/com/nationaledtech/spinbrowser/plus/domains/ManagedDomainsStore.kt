package com.nationaledtech.spinbrowser.plus.domains

import mozilla.components.lib.state.Action
import mozilla.components.lib.state.State
import mozilla.components.lib.state.Store

data class ManagedDomainsState(
    val domainsList: List<ManagedDomain>
) : State

sealed class ManagedDomainsAction : Action {
    data class UpdateDomains(val domains: List<ManagedDomain>) : ManagedDomainsAction()
}

class ManagedDomainsStore(initialState: ManagedDomainsState) :
    Store<ManagedDomainsState, ManagedDomainsAction>(
        initialState, ::managedDomainsStateReducer
    )

private fun managedDomainsStateReducer(
    state: ManagedDomainsState,
    action: ManagedDomainsAction
): ManagedDomainsState {
    return when (action) {
        is ManagedDomainsAction.UpdateDomains -> {
            state.copy(
                domainsList = action.domains
            )
        }
    }
}