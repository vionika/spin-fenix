package com.nationaledtech.spinbrowser.plus.promo

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.mozilla.fenix.FenixApplication
import org.mozilla.fenix.NavGraphDirections
import org.mozilla.fenix.R
import org.mozilla.fenix.databinding.FragmentSpinPlusPromoBinding
import org.mozilla.fenix.ext.application
import org.mozilla.fenix.home.HomeFragmentDirections
import org.mozilla.fenix.settings.SettingsFragmentDirections

class SpinPlusPromoFragment : Fragment(R.layout.fragment_spin_plus_promo) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentSpinPlusPromoBinding.bind(view)
        binding.buyButton.setOnClickListener {
            Navigation.findNavController(view).apply {
                navigate(NavGraphDirections.actionStartupHome())
                navigate(HomeFragmentDirections.actionGlobalSettingsFragment())
                navigate(SettingsFragmentDirections.actionSettingsFragmentToSubscribeSpinPLus())
            }
        }
        binding.continueButton.setOnClickListener {
            Navigation.findNavController(view).navigate(NavGraphDirections.actionStartupHome())
        }
    }

    override fun onResume() {
        super.onResume()

        val manager = (activity?.application as? FenixApplication)?.subscriptionManager
        lifecycleScope.launch(Dispatchers.IO) {
            manager?.userCurrentSubscriptionFlow?.collectLatest { flow ->
                if (flow.hasSubscription) {
                    lifecycleScope.launch(Dispatchers.Main) {
                        view?.let { view ->
                            Navigation.findNavController(view).navigate(NavGraphDirections.actionStartupHome())
                        }
                    }
                }
            }
        }
    }
}
