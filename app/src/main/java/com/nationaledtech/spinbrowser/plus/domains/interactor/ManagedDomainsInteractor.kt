package com.nationaledtech.spinbrowser.plus.domains.interactor

import com.nationaledtech.spinbrowser.plus.domains.ManagedDomain
import com.nationaledtech.spinbrowser.plus.domains.controller.ManagedDomainsController

interface ManagedDomainsInteractor {
    fun onAddDomainClick()
    fun onDeleteDomainClick(domain: ManagedDomain)
}

class DefaultManagedDomainsInteractor(
    private val controller: ManagedDomainsController,
) : ManagedDomainsInteractor {

    override fun onAddDomainClick() {
        controller.handleAddDomainClicked()
    }

    override fun onDeleteDomainClick(domain: ManagedDomain) {
        controller.handleDeleteDomainClicked(domain)
    }
}