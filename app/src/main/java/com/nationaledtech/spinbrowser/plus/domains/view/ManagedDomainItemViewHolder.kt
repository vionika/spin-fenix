package com.nationaledtech.spinbrowser.plus.domains.view

import android.view.View
import com.nationaledtech.spinbrowser.plus.domains.ManagedDomain
import com.nationaledtech.spinbrowser.plus.domains.interactor.ManagedDomainsInteractor
import org.mozilla.fenix.R
import org.mozilla.fenix.databinding.DomainListItemBinding
import org.mozilla.fenix.utils.view.ViewHolder

class ManagedDomainItemViewHolder(
    private val view: View,
    private val interactor: ManagedDomainsInteractor
) : ViewHolder(view) {

    fun bind(domain: ManagedDomain) {
        val binding = DomainListItemBinding.bind(view)

        binding.domainName.text = domain.name

        binding.deleteButton.setOnClickListener {
            interactor.onDeleteDomainClick(domain)
        }
    }

    companion object {
        const val LAYOUT_ID = R.layout.domain_list_item
    }
}
