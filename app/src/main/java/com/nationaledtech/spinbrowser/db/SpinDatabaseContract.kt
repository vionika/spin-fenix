package com.nationaledtech.spinbrowser.db

import android.content.ContentResolver
import android.net.Uri

class SpinDatabaseContract {
    companion object{
        const val DB_NAME = "spin"
        const val DB_VERSION = 4

        private const val SCHEME = "content://"
        const val AUTHORITY = "com.nationaledtech.spinbrowser.db.browser"

        const val PATH_POLICY = "policy"
        const val PATH_HISTORY = "history"
        const val PATH_MANAGED_DOMAINS = "managed_domains"
        const val PATH_GROUP_APPS_STATE = "group_apps_state"

        val BASE_CONTENT_URI = Uri.parse(SCHEME + AUTHORITY)!!
    }

    class PolicyEntry{
        companion object{
            const val TABLE_NAME = "policy"
            const val COLUMN_ID = "_id"
            const val COLUMN_DOMAIN = "domain"
            const val COLUMN_TYPE = "type"

            const val CONTENT_POLICY = "${ContentResolver.CURSOR_DIR_BASE_TYPE}/$AUTHORITY/$PATH_POLICY"
        }
    }

    class HistoryEntry{
        companion object{
            const val TABLE_NAME = "history"
            const val COLUMN_ID = "_id"
            const val COLUMN_TITLE = "title"
            const val COLUMN_URL = "url"
            const val COLUMN_DATE = "date"
            const val COLUMN_MODIFIED = "modified"
            const val COLUMN_FAVICON_URL = "favicon_url"
            const val COLUMN_VISITS = "visits"

            val CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, TABLE_NAME)!!

            const val CONTENT_HISTORY = "${ContentResolver.CURSOR_DIR_BASE_TYPE}/$AUTHORITY/$PATH_HISTORY"
        }
    }

    class ManagedDomainsEntry{
        companion object{
            const val TABLE_NAME = "managed_domains"
            const val COLUMN_ID = "_id"
            const val COLUMN_NAME = "name"
            const val COLUMN_ALLOWED = "is_allowed"
            const val COLUMN_BLOCKED_BY_SPIN = "is_blocked_by_spin"

            val CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, TABLE_NAME)!!

            const val CONTENT_MANAGED_DOMAINS = "${ContentResolver.CURSOR_DIR_BASE_TYPE}/$AUTHORITY/$PATH_MANAGED_DOMAINS"
        }
    }

    class GroupAppsStateEntry{
        companion object{
            const val TABLE_NAME = "group_apps_state"
            const val COLUMN_ID = "_id"
            const val COLUMN_PACKAGE = "package"
            const val COLUMN_STATE = "state"

            const val CONTENT_APP_STATE = "${ContentResolver.CURSOR_DIR_BASE_TYPE}/$AUTHORITY/$PATH_GROUP_APPS_STATE"
        }
    }
}
