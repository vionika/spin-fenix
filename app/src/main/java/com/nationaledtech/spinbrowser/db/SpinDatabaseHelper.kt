package com.nationaledtech.spinbrowser.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.nationaledtech.spinbrowser.db.SpinDatabaseContract.PolicyEntry
import com.nationaledtech.spinbrowser.db.SpinDatabaseContract.HistoryEntry
import com.nationaledtech.spinbrowser.db.SpinDatabaseContract.ManagedDomainsEntry
import com.nationaledtech.spinbrowser.db.SpinDatabaseContract.GroupAppsStateEntry

class SpinDatabaseHelper(context: Context?) : SQLiteOpenHelper(context, SpinDatabaseContract.DB_NAME, null, SpinDatabaseContract.DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        createPolicyTable(db)
        createHistoryTable(db)
        createManagedDomainsTable(db)
        createGroupAppStateTable(db)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.let {
            if (oldVersion < 2) {
                createManagedDomainsTable(it)
            }

            if (oldVersion < 3) {
                createGroupAppStateTable(it)
            }

            if (oldVersion == 3) {
                updateManagedDomainsTableToVersion4(it)
            }
        }
    }

    private fun createPolicyTable(db: SQLiteDatabase?) {
        db?.execSQL("CREATE TABLE ${PolicyEntry.TABLE_NAME} (" +
                "${PolicyEntry.COLUMN_ID} INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "${PolicyEntry.COLUMN_DOMAIN} TEXT, " +
                "${PolicyEntry.COLUMN_TYPE} INTEGER NOT NULL DEFAULT 0);")
    }

    private fun createHistoryTable(db: SQLiteDatabase?) {
        db?.execSQL("CREATE TABLE ${HistoryEntry.TABLE_NAME} (" +
                "${HistoryEntry.COLUMN_ID} INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "${HistoryEntry.COLUMN_TITLE} TEXT, " +
                "${HistoryEntry.COLUMN_URL} TEXT NOT NULL, " +
                "${HistoryEntry.COLUMN_FAVICON_URL} TEXT, " +
                "${HistoryEntry.COLUMN_VISITS} INTEGER NOT NULL DEFAULT 0, " +
                "${HistoryEntry.COLUMN_DATE} INTEGER NOT NULL DEFAULT 0, " +
                "${HistoryEntry.COLUMN_MODIFIED} INTEGER NOT NULL DEFAULT 0);")
    }

    private fun createManagedDomainsTable(db: SQLiteDatabase?) {
        db?.execSQL("CREATE TABLE ${ManagedDomainsEntry.TABLE_NAME} (" +
                "${ManagedDomainsEntry.COLUMN_ID} INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "${ManagedDomainsEntry.COLUMN_NAME} TEXT, " +
                "${ManagedDomainsEntry.COLUMN_ALLOWED} INTEGER NOT NULL DEFAULT 0, " +
                "${ManagedDomainsEntry.COLUMN_BLOCKED_BY_SPIN} INTEGER NOT NULL DEFAULT 0);")
    }

    private fun updateManagedDomainsTableToVersion4(db: SQLiteDatabase) {
        if (!tableExists(db, ManagedDomainsEntry.TABLE_NAME)) {
            createManagedDomainsTable(db)
        }
        if (!columnExists(db, ManagedDomainsEntry.TABLE_NAME, ManagedDomainsEntry.COLUMN_BLOCKED_BY_SPIN)) {
            db.execSQL("ALTER TABLE ${ManagedDomainsEntry.TABLE_NAME} " +
                    "ADD COLUMN ${ManagedDomainsEntry.COLUMN_BLOCKED_BY_SPIN} INTEGER NOT NULL DEFAULT 0")
        }
    }

    private fun createGroupAppStateTable(db: SQLiteDatabase?) {
        db?.execSQL("CREATE TABLE ${GroupAppsStateEntry.TABLE_NAME} (" +
                "${GroupAppsStateEntry.COLUMN_ID} INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "${GroupAppsStateEntry.COLUMN_PACKAGE} TEXT UNIQUE ON CONFLICT REPLACE, " +
                "${GroupAppsStateEntry.COLUMN_STATE} INTEGER NOT NULL DEFAULT 0);")
    }

    private fun tableExists(db: SQLiteDatabase, tableName: String): Boolean {
        db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name=?", arrayOf(tableName)).use {
            return it.count > 0
        }
    }

    private fun columnExists(db: SQLiteDatabase, tableName: String, columnName: String): Boolean {
        val cursor = db.rawQuery("PRAGMA table_info($tableName)", null)
        cursor.use {
            if (it.moveToFirst()) {
                do {
                    val currentColumn = it.getString(it.getColumnIndexOrThrow("name"))
                    if (currentColumn.equals(columnName, ignoreCase = true)) {
                        return true
                    }
                } while (it.moveToNext())
            }
        }
        return false
    }
}
