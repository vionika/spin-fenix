package com.nationaledtech.spinbrowser.db

import android.content.ContentProvider
import android.content.ContentUris
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import android.util.Log
import com.nationaledtech.spinbrowser.db.SpinDatabaseContract.PolicyEntry
import com.nationaledtech.spinbrowser.db.SpinDatabaseContract.HistoryEntry
import com.nationaledtech.spinbrowser.db.SpinDatabaseContract.ManagedDomainsEntry
import com.nationaledtech.spinbrowser.db.SpinDatabaseContract.GroupAppsStateEntry

class SpinContentProvider : ContentProvider() {

    private val policy = 1
    private val history = 2
    private val managedDomains = 3
    private val groupAppsState = 4

    private lateinit var dbHelper: SpinDatabaseHelper

    private val uriMatcher: UriMatcher by lazy {
        UriMatcher(UriMatcher.NO_MATCH).apply {
            addURI(SpinDatabaseContract.AUTHORITY, SpinDatabaseContract.PATH_POLICY, policy)
            addURI(SpinDatabaseContract.AUTHORITY, SpinDatabaseContract.PATH_HISTORY, history)
            addURI(SpinDatabaseContract.AUTHORITY, SpinDatabaseContract.PATH_MANAGED_DOMAINS, managedDomains)
            addURI(SpinDatabaseContract.AUTHORITY, SpinDatabaseContract.PATH_GROUP_APPS_STATE, groupAppsState)
        }
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        val db = dbHelper.writableDatabase
        val count = when (uriMatcher.match(uri)) {
            policy -> {
                db.delete(
                        PolicyEntry.TABLE_NAME,
                        selection,
                        selectionArgs
                )
            }
            history -> {
                db.delete(
                    HistoryEntry.TABLE_NAME,
                    selection,
                    selectionArgs
                )
            }
            managedDomains -> {
                db.delete(
                    ManagedDomainsEntry.TABLE_NAME,
                    selection,
                    selectionArgs
                )
            }
            groupAppsState -> {
                db.delete(
                    GroupAppsStateEntry.TABLE_NAME,
                    selection,
                    selectionArgs
                )
            }
            else -> {
                throw IllegalArgumentException("Can't delete URI $uri")
            }
        }
        if (count !=0 ) {
            context?.contentResolver?.notifyChange(uri, null)
        }
        return count
    }

    override fun getType(uri: Uri): String {
        return when (uriMatcher.match(uri)) {
            policy -> PolicyEntry.CONTENT_POLICY
            history -> HistoryEntry.CONTENT_HISTORY
            managedDomains -> ManagedDomainsEntry.CONTENT_MANAGED_DOMAINS
            groupAppsState -> GroupAppsStateEntry.CONTENT_APP_STATE
            else -> throw IllegalArgumentException("Unknown URI $uri")
        }
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        val db = dbHelper.writableDatabase
        when (uriMatcher.match(uri)) {
            policy -> {
                val id = db.insert(PolicyEntry.TABLE_NAME, null, values)
                if (id == (-1).toLong()) {
                    Log.e("insertMethod", "Insertion of data in the table failed for $uri")
                    return null
                }
                context?.contentResolver?.notifyChange(uri,null)
                return ContentUris.withAppendedId(uri, id)
            }
            history -> {
                val id = db.insert(HistoryEntry.TABLE_NAME, null, values)
                if (id == (-1).toLong()) {
                    Log.e("insertMethod", "Insertion of data in the table failed for $uri")
                    return null
                }
                context?.contentResolver?.notifyChange(uri,null)
                return ContentUris.withAppendedId(uri, id)
            }
            managedDomains -> {
                val id = db.insert(ManagedDomainsEntry.TABLE_NAME, null, values)
                if (id == (-1).toLong()) {
                    Log.e("insertMethod", "Insertion of data in the table failed for $uri")
                    return null
                }
                context?.contentResolver?.notifyChange(uri,null)
                return ContentUris.withAppendedId(uri, id)
            }
            groupAppsState -> {
                val id = db.insert(GroupAppsStateEntry.TABLE_NAME, null, values)
                if (id == (-1).toLong()) {
                    Log.e("insertMethod", "Insertion of data in the table failed for $uri")
                    return null
                }
                context?.contentResolver?.notifyChange(uri,null)
                return ContentUris.withAppendedId(uri, id)
            }
            else -> {
                throw IllegalArgumentException("Insertion of data in the table failed for $uri")
            }
        }
    }

    override fun onCreate(): Boolean {
        dbHelper = SpinDatabaseHelper(context)
        return true
    }

    override fun query(
            uri: Uri, projection: Array<String>?, selection: String?,
            selectionArgs: Array<String>?, sortOrder: String?
    ): Cursor? {
        val db = dbHelper.readableDatabase
        val cursor = when (uriMatcher.match(uri)) {
            policy -> {
                db.query(
                        PolicyEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                )
            }
            history -> {
                db.query(
                    HistoryEntry.TABLE_NAME,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    sortOrder
                )
            }
            managedDomains -> {
                db.query(
                    ManagedDomainsEntry.TABLE_NAME,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    sortOrder
                )
            }
            groupAppsState -> {
                db.query(
                    GroupAppsStateEntry.TABLE_NAME,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    sortOrder
                )
            }
            else -> {
                throw IllegalArgumentException("Can't query incorrect URI $uri")
            }
        }
        cursor.setNotificationUri(context?.contentResolver,uri)
        return cursor
    }

    override fun update(
            uri: Uri, values: ContentValues?, selection: String?,
            selectionArgs: Array<String>?
    ): Int {
        val db = dbHelper.writableDatabase
        val count=  when (uriMatcher.match(uri)) {
            policy -> {
                db.update(
                        PolicyEntry.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs
                )
            }
            history -> {
                db.update(
                    HistoryEntry.TABLE_NAME,
                    values,
                    selection,
                    selectionArgs
                )
            }
            managedDomains -> {
                db.update(
                    ManagedDomainsEntry.TABLE_NAME,
                    values,
                    selection,
                    selectionArgs
                )
            }
            groupAppsState -> {
                db.update(
                    GroupAppsStateEntry.TABLE_NAME,
                    values,
                    selection,
                    selectionArgs
                )
            }
            else -> {
                throw IllegalArgumentException("Can't update URI $uri")
            }
        }
        if (count !=0 ) {
            context?.contentResolver?.notifyChange(uri, null)
        }
        return count
    }
}
