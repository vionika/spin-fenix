package com.nationaledtech.spinbrowser

import android.content.Context
import android.graphics.BitmapFactory
import com.nationaledtech.spinbrowser.state.GroupAppsStateReader
import mozilla.components.browser.state.search.SearchEngine
import mozilla.components.browser.state.state.searchEngines
import org.mozilla.fenix.R
import org.mozilla.fenix.ext.components


object SpinConfigurator {
    val supportedSearchEngines = listOf("google", "bing", "duckduckgo", "ecosia")
    var customSearchEngines = listOf<SearchEngine>()

    private val appStateReader = GroupAppsStateReader()

    fun setup(context: Context) {
        addCustomSearchEngines(context)
        deleteUnsupportedSearchEngines(context)
    }

    fun isAnyGroupAppLicensed(context: Context): Boolean {
        return appStateReader.hasActiveApp(context)
    }

    private fun addCustomSearchEngines(context: Context) {
        val ecosiaIcon = BitmapFactory.decodeResource(context.resources, R.drawable.ic_ecosia)
        val ecosiaSearchString = "https://www.ecosia.org/search?q={searchTerms}"
        val ecosiaSearchEngine = SearchEngine(
            id = "ecosia",
            name = "Ecosia",
            icon = ecosiaIcon,
            type = SearchEngine.Type.CUSTOM,
            resultUrls = listOf(ecosiaSearchString),
            isGeneral = true
        )

        customSearchEngines = listOf(ecosiaSearchEngine)

        if (!SpinSupportUtils.isEcosiaAddedAsDefault(context)) {
            context.components.useCases.searchUseCases.addSearchEngine(ecosiaSearchEngine)
            SpinSupportUtils.setEcosiaAddedAsDefault(context)
        }
    }

    private fun deleteUnsupportedSearchEngines(context: Context) {
        val store = context.components.core.store
        val state = store.state.search
        state.searchEngines.forEach { engine ->
            if (!supportedSearchEngines.contains(engine.name.lowercase())) {
                context.components.useCases.searchUseCases.removeSearchEngine(engine)
            }
        }
    }
}
