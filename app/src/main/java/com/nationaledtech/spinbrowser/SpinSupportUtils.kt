package com.nationaledtech.spinbrowser

import android.content.Context

import android.os.StrictMode
import androidx.preference.PreferenceManager


object SpinSupportUtils {
    const val PROMO_SHOWN_VERSION_PREFERENCE_KEY = "spin_plus_promo"
    const val PROMO_CURRENT_VERSION = 1

    const val PASSCODE_PREFERENCE_KEY = "spin_plus_passcode"

    const val PROTECT_ADDONS_WITH_PASSCODE_PREFERENCE_KEY = "spin_plus_protect_addons_with_passcode"

    const val ADD_DEFAULT_SEARCH_ENGINE_ECOSIA_PREFERENCE_KEY = "add_default_search_engine_ecosia"

    fun getPrivacyPageUrl(): String {
        return "https://spinsafebrowser.com/spinprivacypolicy/"
    }

    fun getSupportPageUrl(): String {
        return "https://community.useboomerang.com/hc/en-us/sections/360000687332-Filter-Chrome-and-SPIN-Safe-Browser"
    }

    fun getRightsUrl(): String {
        return "https://spinsafebrowser.com/spin-about-your-rights/"
    }

    fun getTrackingProtectionInfoUrl(): String {
        return "https://community.useboomerang.com/hc/en-us/articles/360056471191"
    }

    fun getFilterChromeGoolePlayUrl(): String {
        return "https://play.google.com/store/apps/details?id=com.nationaledtech.managespin"
    }

    fun getBoomerangGoolePlayUrl(): String {
        return "https://play.google.com/store/apps/details?id=com.nationaledtech.Boomerang"
    }

    fun isPasscodeSet(context: Context): Boolean {
        var string: String? = null
        allowReads {
            string = PreferenceManager.getDefaultSharedPreferences(context).getString(PASSCODE_PREFERENCE_KEY, null)
        }
        return !string.isNullOrBlank()
    }

    fun setEncodedPasscode(context: Context, encodedString: String?) {
        allowWrites {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            if (encodedString == null) {
                prefs.edit().remove(PASSCODE_PREFERENCE_KEY).commit()
            } else {
                prefs.edit().putString(PASSCODE_PREFERENCE_KEY, encodedString).commit()
            }
        }
    }

    fun getEncodedPasscode(context: Context): String? {
        var passcode: String? = null
        allowReads {
            passcode = PreferenceManager.getDefaultSharedPreferences(context).getString(PASSCODE_PREFERENCE_KEY, null)
        }
        return passcode
    }

    fun getBoomerangSiteUrl(): String {
        return "https://useboomerang.com/"
    }

    fun getFilterChromeSiteUrl(): String {
        return "https://filterchrome.com/"
    }

    fun getSpinSiteUrl(): String {
        return "https://spinsafebrowser.com/"
    }

    fun shouldShowPromo(context: Context): Boolean {
        var version = 0
        allowReads {
            version = PreferenceManager.getDefaultSharedPreferences(context).getInt(
                PROMO_SHOWN_VERSION_PREFERENCE_KEY, 0,
            )
        }
        return version < PROMO_CURRENT_VERSION
    }

    fun setShowPromoCurrentVersionDone(context: Context) {
        allowWrites {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            prefs.edit().putInt(PROMO_SHOWN_VERSION_PREFERENCE_KEY, PROMO_CURRENT_VERSION).commit()
        }
    }

    fun shouldProtectAddonsWithPasscode(context: Context): Boolean {
        var shouldProtect = true
        allowReads {
            shouldProtect = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(
                PROTECT_ADDONS_WITH_PASSCODE_PREFERENCE_KEY, true,
            )
        }
        return shouldProtect
    }

    fun setShouldProtectAddonsWithPasscode(context: Context, shouldProtect: Boolean) {
        allowWrites {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            prefs.edit().putBoolean(PROTECT_ADDONS_WITH_PASSCODE_PREFERENCE_KEY, shouldProtect).commit()
        }
    }

    fun isEcosiaAddedAsDefault(context: Context): Boolean {
        var result = false
        allowReads {
            result = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(
                ADD_DEFAULT_SEARCH_ENGINE_ECOSIA_PREFERENCE_KEY, false,
            )
        }
        return result
    }

    fun setEcosiaAddedAsDefault(context: Context) {
        allowWrites {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            prefs.edit().putBoolean(ADD_DEFAULT_SEARCH_ENGINE_ECOSIA_PREFERENCE_KEY, true).commit()
        }
    }

    private fun <T> allowReads(block: () -> T): T {
        val oldPolicy = StrictMode.allowThreadDiskReads()
        try {
            return block()
        } finally {
            StrictMode.setThreadPolicy(oldPolicy)
        }
    }

    private fun <T> allowWrites(block: () -> T): T {
        val oldPolicy = StrictMode.allowThreadDiskWrites()
        try {
            return block()
        } finally {
            StrictMode.setThreadPolicy(oldPolicy)
        }
    }
}
