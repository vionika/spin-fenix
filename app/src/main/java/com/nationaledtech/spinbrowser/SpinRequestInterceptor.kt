package com.nationaledtech.spinbrowser

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.net.UrlQuerySanitizer
import android.os.Build
import android.util.Patterns
import android.webkit.URLUtil
import androidx.lifecycle.lifecycleScope
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.json.responseJson
import com.nationaledtech.spinbrowser.actions.SpinActions
import com.nationaledtech.spinbrowser.pages.SpinErrorPages
import com.nationaledtech.spinbrowser.plus.domains.DefaultManagedDomainsStorage
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collectLatest
import mozilla.components.browser.errorpages.ErrorPages
import mozilla.components.browser.errorpages.ErrorType
import mozilla.components.concept.engine.EngineSession
import mozilla.components.concept.engine.request.RequestInterceptor
import org.mozilla.fenix.AppRequestInterceptor
import org.mozilla.fenix.FenixApplication
import org.mozilla.fenix.ext.application
import org.mozilla.fenix.ext.components
import java.lang.ref.WeakReference
import java.net.IDN
import java.net.MalformedURLException
import java.net.URI
import java.net.URL
import java.util.*
import java.util.regex.Pattern

class SpinRequestInterceptor(
    private val context: Context
) : RequestInterceptor {

    private var navController: WeakReference<NavController>? = null
    private var appRequestInterceptor: AppRequestInterceptor = AppRequestInterceptor(context)

    private val managedDomainsStorage = DefaultManagedDomainsStorage(context)

    private val googlePatten = Pattern.compile(
        "^(https?:\\/\\/)?(w{3}\\.)?(images\\.)?google\\.",
        Pattern.CASE_INSENSITIVE
    )
    private val bingPatten =
        Pattern.compile("^(https?:\\/\\/)?(w{3}\\.)?(.+\\.)?bing\\.", Pattern.CASE_INSENSITIVE)
    private val yahooPatten =
        Pattern.compile("^(https?:\\/\\/)?(w{3}\\.)?(.+\\.)?yahoo\\.", Pattern.CASE_INSENSITIVE)
    private val duckDuckGoPatten =
        Pattern.compile("^(https?:\\/\\/)?(w{3}\\.)?(.+\\.)?duckduckgo\\.", Pattern.CASE_INSENSITIVE)
    private val googleTranslatePatten =
        Pattern.compile("^(https?:\\/\\/)?(w{3}\\.)?(.+\\.)?translate.goog", Pattern.CASE_INSENSITIVE)
    private val ecosiaPatten =
        Pattern.compile("^(https?:\\/\\/)?(w{3}\\.)?(.+\\.)?ecosia\\.", Pattern.CASE_INSENSITIVE)

    private var whiteListPattern: Pattern? = null
    private var blackListPattern: Pattern? = null

    private val defaultProhibitedCategoriesList = listOf(1011, 1062, 10002, 10007, 1031, 1058)
    private val unknownCategoriesList: List<Int> = emptyList()

    private val licenseInfoUrl = "https://spinsafebrowser.com/spin-safe-browser-licenses/"

    private val POLICY_CONTENT_URL = "content://%s.db.browser/policy"
    private val HISTORY_CONTENT_URL = "content://%s.db.browser/history"

    private val PROVIDER_URL = "content://%s.db.browser"
    private val API_KEY = 10
    private val DEFAULT_KEY = "oz2erssx768cHfzDMOO1PsyIz2EaJsyDppqrwmHckoHsrGBOJ2tPkA=="
    private val BLACK_LIST = 0
    private val WHITE_LIST = 1
    private val FILTER_CATEGORY = 20
    private var apiKey: String? = null

    private val SPIN_ACTION_SCHEME = "spin-action"

    private var prohibitedCategories: List<Int> = defaultProhibitedCategoriesList

    private val trustedApps = listOf(
        "com.nationaledtech.Boomerang",
        "com.nationaledtech.spinmanagement"
    )

    private val allowedDomains = listOf(
        "parentalboard.com",
        "mobilement.com",
        "spinbrowse.com",
        "nationaledtech.com"
    )

    private val cachedDomains = mutableMapOf<String, Int>()

    /* Columns */
    private val DOMAIN = "domain"
    private val TYPE = "type"

    val job = Job()
    val ioScope = CoroutineScope(Dispatchers.IO + job)

    private var hasSubscription = false

    init {
        observeSubscription()
        grantPermissions()
    }

    fun setNavigationController(navController: NavController) {
        this.navController = WeakReference(navController)
    }

    override fun interceptsAppInitiatedRequests(): Boolean {
        return true
    }

    override fun onLoadRequest(
        engineSession: EngineSession,
        uri: String,
        lastUri: String?,
        hasUserGesture: Boolean,
        isSameDomain: Boolean,
        isRedirect: Boolean,
        isDirectNavigation: Boolean,
        isSubframeRequest: Boolean
    ): RequestInterceptor.InterceptionResponse? {
        val scheme = Uri.parse(uri).normalizeScheme().scheme

        if (scheme.equals("resource")) {
            return null
        }

        if (scheme.equals(SPIN_ACTION_SCHEME)) {
            val action = Uri.parse(uri).host
            if (action.equals("allow")) {
                val url = Uri.parse(uri).getQueryParameter("url")
                val domain = url?.let { getDomainName(it) }

                domain?.let {
                    cachedDomains[domain]?.let { category ->
                        if (unknownCategoriesList.contains(category)) {
                            managedDomainsStorage.addDomain(domain = domain, isAllowed = true, isBlockedBySpin = true)
                            return RequestInterceptor.InterceptionResponse.Url(url)
                        }
                    }
                }

                val blockPage = formatBlockedUrlInfoPageLink(domain ?: "", 0, uri)
                return RequestInterceptor.InterceptionResponse.Url(blockPage)
            } else if (action.equals("block")) {
                val url = Uri.parse(uri).getQueryParameter("url")
                val domain = url?.let { getDomainName(it) }
                if (domain != null) {
                    managedDomainsStorage.addDomain(domain = domain, isAllowed = false, isBlockedBySpin = true)
                    return RequestInterceptor.InterceptionResponse.Url(url)
                }
            } else if (action.equals("upgrade")) {
                LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(SpinActions.SHOW_SPIN_SUBSCRIPTION))
                return null
            }
        }

        val domainName = getDomainName(uri)
        if (domainName.isBlank()) {
            return null
        }
        if (isDomainProhibited(domainName)) {
            val blockPage = formatBlockedUrlInfoPageLink(domainName, 0, uri)
            return RequestInterceptor.InterceptionResponse.Url(blockPage)
        }

        if (isDomainAllowed(domainName)) {
            return null
        }

        if (isSearchEngine(uri)) {
            return if (isSafeSearchEngine(uri)) {
                null
            } else {
                RequestInterceptor.InterceptionResponse.Url(makeSafeSearchUrl(uri))
            }
        }

        if (!isSubframeRequest) {
            if (uri.contains("spinbrowse.com")) {
                return null
            }

            if (uri.startsWith("about:")) {
                if (uri == "about:license") {
                    return RequestInterceptor.InterceptionResponse.Url(licenseInfoUrl)
                }

                return RequestInterceptor.InterceptionResponse.Deny
            }

            checkRequestSafety(engineSession, uri, isSameDomain)?.let { response ->
                return response
            }

            // Check urls in uri query
            val sanitizer = UrlQuerySanitizer()
            sanitizer.allowUnregisteredParamaters = true
            sanitizer.parseUrl(uri)
            sanitizer.parameterList.forEach {
                if (isValidUrl(it.mValue)) {
                    checkUrlSafety(engineSession, it.mValue)
                }
            }
        }

        return appRequestInterceptor
            .onLoadRequest(
                engineSession,
                uri,
                lastUri,
                hasUserGesture,
                isSameDomain,
                isRedirect,
                isDirectNavigation,
                isSubframeRequest
            )
    }

    override fun onErrorRequest(
        session: EngineSession,
        errorType: ErrorType,
        uri: String?
    ): RequestInterceptor.ErrorResponse? {
        return appRequestInterceptor.onErrorRequest(session, errorType, uri)
    }

    private fun checkRequestSafety(
        engineSession: EngineSession,
        uri: String,
        isSameDomain: Boolean
    ): RequestInterceptor.InterceptionResponse? {
        if (!isValidUrl(uri)) {
            return null
        }

        if (isSameDomain) {
            return null
        }

        val domainName = getDomainName(uri)
        cachedDomains[domainName]?.let { category ->
            if (prohibitedCategories.contains(category)) {
                val blockPage = formatBlockedUrlInfoPageLink(domainName, category, uri)
                return RequestInterceptor.InterceptionResponse.Url(blockPage)
            }

            return null
        }

        ioScope.launch {
            examineDomain(engineSession, uri)
        }

        return null
    }

    private fun checkUrlSafety(engineSession: EngineSession, url: String) {
        val domainName = getDomainName(url)
        if (domainName.isNullOrBlank()) {
            return
        }

        cachedDomains[domainName]?.let { category ->
            if (prohibitedCategories.contains(category)) {
                val blockPage = formatBlockedUrlInfoPageLink(domainName, category, url)
                loadBlockPage(engineSession, blockPage)
            }

            return
        }

        if (isDomainAllowed(domainName)) {
            return
        }

        ioScope.launch {
            examineDomain(engineSession, url)
        }
    }

    private fun loadBlockPage(engineSession: EngineSession, url: String) {
        ioScope.launch(Dispatchers.Main) {
            // Remove blocked page from history stack and load block url
            engineSession.stopLoading()
            val flags: EngineSession.LoadUrlFlags = EngineSession.LoadUrlFlags.select(EngineSession.LoadUrlFlags.LOAD_FLAGS_REPLACE_HISTORY)
            engineSession.loadUrl(url, null, flags)
        }
    }

    private fun isValidUrl(uri: String): Boolean {
        try {
            URL(uri)
            return URLUtil.isValidUrl(uri) && Patterns.WEB_URL.matcher(uri).matches()
        } catch (ignored: MalformedURLException) {
        }
        return false
    }

    private fun isDomainProhibited(domainName: String): Boolean {
        val onlyBlockedBySpin = if (hasSubscription) null else true
        val prohibitedManagedDomains =
            managedDomainsStorage.getAllDomains(false, onlyBlockedBySpin).map { it.name }
        if (prohibitedManagedDomains.contains(domainName)) {
            return true
        }

        ensurePolicy()

        return isDomainProhibitedByPolicy(domainName)
    }

    private fun isDomainAllowed(domainName: String): Boolean {
        val onlyBlockedBySpin = if (hasSubscription) null else true
        val allowedManagedDomains =
            managedDomainsStorage.getAllDomains(true, onlyBlockedBySpin).map { it.name }
        if (allowedManagedDomains.contains(domainName)) {
            return true
        }

        if (allowedDomains.contains(domainName)) {
            return true
        }

        if (whiteListPattern?.matcher(domainName)?.matches() == true) {
            return true
        }

        return false
    }

    private fun isDomainProhibitedByPolicy(domainName: String): Boolean {
        if (blackListPattern?.matcher(domainName)?.matches() == true) {
            return true
        }

        return false
    }

    private fun ensurePolicy() {
        whiteListPattern = getPolicyPatten(WHITE_LIST)
        blackListPattern = getPolicyPatten(BLACK_LIST)

        apiKey = getValue(API_KEY) ?: DEFAULT_KEY

        val categories = getIntList(FILTER_CATEGORY) ?: defaultProhibitedCategoriesList
        if (prohibitedCategories != categories) {
            prohibitedCategories = categories
            cachedDomains.clear()
        }
    }

    private fun getPolicyPatten(type: Int): Pattern? {
        val policyContentUrl = POLICY_CONTENT_URL.format(context.packageName)
        val cursor = context.contentResolver.query(
            Uri.parse(policyContentUrl),
            arrayOf(DOMAIN),
            "$TYPE = ?",
            arrayOf("$type"),
            null
        )

        cursor.use {
            if (it != null && it.moveToFirst()) {
                var domains = LinkedList<String>()
                do {
                    val domain = it.getString(0)
                    if (domain.isNotBlank()) {
                        domains.add(domain)
                    }
                } while (it.moveToNext())

                val patternString = join(domains, "|")
                    .replace(".", "\\.")
                    .replace("*", ".*")
                    .replace("?", ".?")
                return Pattern.compile("^($patternString)\$")
            }
        }

        return null
    }

    private fun getValue(type: Int): String? {
        val policyContentUrl = POLICY_CONTENT_URL.format(context.packageName)
        val cursor = context.contentResolver.query(
            Uri.parse(policyContentUrl),
            arrayOf(DOMAIN),
            "$TYPE = ?",
            arrayOf("$type"),
            null
        )

        cursor.use {
            if (it != null && it.moveToFirst()) {
                return it.getString(0)
            }
        }

        return null
    }

    private fun getIntList(type: Int): List<Int>? {
        val policyContentUrl = POLICY_CONTENT_URL.format(context.packageName)
        val cursor = context.contentResolver.query(
            Uri.parse(policyContentUrl),
            arrayOf(DOMAIN),
            "$TYPE = ?",
            arrayOf("$type"),
            null
        )

        cursor.use {
            if (it != null && it.moveToFirst()) {
                var result = LinkedList<Int>()
                do {
                    val value = it.getString(0)
                    if (value.isNotBlank()) {
                        val intValue = value.toIntOrNull()
                        if (intValue != null) {
                            result.add(Integer.valueOf(value))
                        }
                    }
                } while (it.moveToNext())

                return result
            }
        }

        return null
    }

    private fun examineDomain(engineSession: EngineSession, uri: String) {
        val examineUrl = "https://www.vionika.com/services/examine/domain"
        val domainName = getDomainName(uri)
        val jsonBody = String.format(
            "{\"domainName\":\"%s\", \"key\":\"%s\", \"v\":\"1\"}",
            domainName,
            apiKey
        )

        val (_, response, result) = Fuel.post(examineUrl)
            .header("Content-Type" to "application/json", "Accept" to "application/json")
            .body(jsonBody)
            .responseJson()

        if (response.responseMessage == "OK" && response.statusCode == 200) {
            val categories = result.get().obj().getJSONArray("categories")
            for (i in 0 until categories.length()) {
                val category = categories.getInt(i)
                cachedDomains[domainName] = category

                if (!SpinConfigurator.isAnyGroupAppLicensed(context) && unknownCategoriesList.contains(category)) {
                    val errorPageUri = SpinErrorPages.createUrlEncodedUnknownWebsitePage(
                        context = context,
                        url = uri,
                        hasSubscription = hasSubscription
                    )
                    engineSession.loadUri(errorPageUri)
                    break
                } else if (prohibitedCategories.contains(category)) {
                    val blockPage = formatBlockedUrlInfoPageLink(domainName, category, uri)
                    loadBlockPage(engineSession, blockPage)
                    break
                }
            }
        }
    }

    fun getDomainName(url: String): String {
        var asciiName = url
        try {
            val uri = URI(url)
            var domainName: String = uri.toURL().host

            domainName = if (domainName.startsWith("www.")) domainName.substring(4) else domainName
            domainName = parseGoogleTranslateUrl(domainName)

            // need to return ascii name to detect and block non latin restricted domains
            asciiName = try {
                IDN.toASCII(domainName)
            } catch (exception: Exception) {
                domainName
            }
        } catch (exception: Exception) {
        }

        return asciiName.lowercase()
    }

    private fun parseGoogleTranslateUrl(url: String): String {
        if (!googleTranslatePatten.matcher(url).find()) {
            return url
        }

        return url.substringBefore(".translate.goog").replace("-", ".").replace("..", "-")
    }

    private fun formatBlockedUrlInfoPageLink(
        domainName: String,
        category: Int,
        encodedUrl: String = ""
    ): String {
        return "https://www.spinbrowse.com/blocked/?domain=$domainName&category=$category&managed=${isManagedByVionikaApps()}&url=$encodedUrl"
    }

    private fun isManagedByVionikaApps(): Int {
        return if (apiKey.equals(DEFAULT_KEY)) 1 else 0
    }

    private fun isSearchEngine(url: String): Boolean {
        if (!isValidUrl(url)) {
            return false
        }

        val hostName: String = URL(url).host
        if (hostName.isNullOrBlank()) {
            return false
        }

        return (googlePatten.matcher(hostName).find()
                || yahooPatten.matcher(hostName).find()
                || bingPatten.matcher(hostName).find()
                || duckDuckGoPatten.matcher(hostName).find()
                || ecosiaPatten.matcher(hostName).find())
    }

    private fun isSafeSearchEngine(url: String): Boolean {
        if (googlePatten.matcher(url).find()) {
            if (!(url.contains("/search") && url.contains("q="))
                || (url.contains("safe=strict") && countMatches(url, "safe=") == 1)) {
                    return true
            }
        } else if (yahooPatten.matcher(url).find()) {
            if ((!url.contains("/search?p=")
                && !url.contains("/search/")
                && !url.contains("/search;"))
                || (url.contains("vm=r") && countMatches(url, "vm=") == 1)) {
                return true
            }
        } else if (bingPatten.matcher(url).find()) {
            if ((!url.contains("?q=") && !url.contains("&q="))
                || (url.contains("adlt=strict") && countMatches(url, "adlt=") == 1)) {
                return true
            }
        } else if (duckDuckGoPatten.matcher(url).find()) {
            if ((url.contains("/?") && !url.contains("q="))
                || (url.contains("kp=1") && countMatches(url, "kp=") == 1)
                || (!url.contains("/?") && url.contains(".html"))) {
                return true
            }
        } else if (ecosiaPatten.matcher(url).find()) {
            if (url.contains("sfs=true") && countMatches(url, "sfs=") == 1) {
                return true
            }

            val query = Uri.parse(url).query
            if (query == null || query?.isEmpty() == true) {
                return true
            }
        }

        return false
    }

    private fun makeSafeSearchUrl(url: String): String {
        val uri = Uri.parse(url)

        if (googlePatten.matcher(url).find()) {
            val safeUri = uri.addUriParameter("safe", "strict")
            return safeUri.toString()
        } else if (yahooPatten.matcher(url).find()) {
            val safeUri = uri.addUriParameter("vm", "r")
            return safeUri.toString()
        } else if (bingPatten.matcher(url).find()) {
            val safeUri = uri.addUriParameter("adlt", "strict")
            return safeUri.toString().replace("&format=snrjson", "")
        } else if (duckDuckGoPatten.matcher(url).find()) {
            val safeUri = uri.addUriParameter("kp", "1")
            return safeUri.toString()
        } else if (ecosiaPatten.matcher(url).find()) {
            val safeUri = uri.addUriParameter("sfs", "true").addUriParameter("addon", "spinbrowserdefault").addUriParameter("tt", "662263be")
            return safeUri.toString()
        }

        return url
    }

    private fun grantPermissions() {
        val contentUrl = PROVIDER_URL.format(context.packageName)
        val policyContentUrl = POLICY_CONTENT_URL.format(context.packageName)
        val historyContentUrl = HISTORY_CONTENT_URL.format(context.packageName)

        var permissions =
            Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            permissions =
                permissions or (Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION or Intent.FLAG_GRANT_PREFIX_URI_PERMISSION)
        }

        trustedApps.forEach { packageName ->
            context.grantUriPermission(packageName, Uri.parse(contentUrl), permissions)
            context.grantUriPermission(packageName, Uri.parse(policyContentUrl), permissions)
            context.grantUriPermission(packageName, Uri.parse(historyContentUrl), permissions)
        }
    }

    private fun observeSubscription() {
        val manager = (context.application).subscriptionManager

        ioScope.launch(Dispatchers.IO) {
            manager.userCurrentSubscriptionFlow.collectLatest {
                hasSubscription = it.hasSubscription
            }
        }
    }

    fun join(s: Collection<String>, delimiter: String): String {
        val builder = StringBuilder()
        val iterator = s.iterator()
        while (iterator.hasNext()) {
            builder.append(iterator.next())
            if (!iterator.hasNext()) {
                break
            }
            builder.append(delimiter)
        }
        return builder.toString()
    }
}

fun Uri.addUriParameter(key: String, newValue: String): Uri {
    val params = queryParameterNames
    val newUri = buildUpon().clearQuery()
    var isSameParamPresent = false
    for (param in params) {
        // if same param is present override it, otherwise add the old param back
        newUri.appendQueryParameter(
            param,
            if (param == key) newValue else getQueryParameter(param)
        )
        if (param == key) {
            // make sure we do not add new param again if already overridden
            isSameParamPresent = true
        }
    }
    if (!isSameParamPresent) {
        // never overrode same param so add new passed value now
        newUri.appendQueryParameter(
            key,
            newValue
        )
    }
    return newUri.build()
}

fun countMatches(string: String, pattern: String): Int {
    return string.split(pattern)
        .dropLastWhile { it.isEmpty() }
        .toTypedArray().size - 1
}
